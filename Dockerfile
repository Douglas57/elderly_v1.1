FROM python:3.6-slim

MAINTAINER douglasirungu57@gmail.com

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
ENV APPWORKINGDIR=/src
ENV SERVERPORT=8000
ENV TOTALWORKERS=4


COPY . $APPWORKINGDIR

WORKDIR $APPWORKINGDIR

COPY requirements.txt ./

RUN pip3 install -r requirements.txt

#ENTRYPOINT gunicorn edms.wsgi:application - bind  0.0.0.0:$SERVERPORT
#ENTRYPOINT python3 manage.py runserver 0.0.0.0:$SERVERPORT


#EXPOSE $SERVERPORT
