from django.test import TestCase


class YourTestClass(TestCase):
    @classmethod
    def setUpTestData(cls):
        print("setUpTestData: Run once to setup non-modified data for all class methods")
        pass

    def setUp(self):
        print("setUp: Run once for every test method to setup clean data")
        pass

    def test_false_is_false(self):
        print("method: test_false_is_false")
        self.assertFalse(False)

    def test_false_is_true(self):
        print("method: test_false_is_true")
        self.assertTrue(False)

    def test_one_plus_one_equals_two(self):
        print("method: test_one_plus_one_two")
        self.assertEqual(1 + 1, 2)
