
from django.contrib.auth.views import LogoutView, PasswordChangeView
from django.urls import path, re_path, include

from django.conf.urls import url
from django.views.decorators.csrf import csrf_exempt

from .manual_splitting import split_pdfs
from .view.statistics import statistics
from .views import (
    registry_submit, AdminView, FileTypeDelete,
    DocumentTranscribe,
    CleanSplit,
    get_document_and_document_type,
    UserListView, UserDetailView, UserUpdateView, LoggedInUserListView,
    UserDeleteView, GroupListView, GroupUpdateView, user_create, Login,
    add_group, update_document_content,
    validate_document_content,
    receiver_batch_submit,
    start_scanning, start_qa, start_validate, change_password, unassign_files_from_users, open_state_of_files,
    close_state_of_files, null_page, start_process)
from .view.user import reset_default_password
from app.view.file import (
    FilesView, DocumentFileCreate, DocumentFileList, file_search, DocumentFileSearchedList, inspect_docs,
    RejectedDocumentFileList, RequestFile, FileDeleteView, FileUpdateView, CompleteFileList, file_internals,
    set_max_assignable, DocumentFileListQA)
from .view.registry import (registry_submit_to_receiver, change_file_status_to_accept, change_file_status_to_accept_ajax,
                            change_file_status_to_reject,change_document_status_to_accept,
                            change_document_status_to_reject, return_rectified_file)
from .view.receiver import select_file
from app.view.batch import (BatchListView, create_batch,search_batch,BatchDeleteView,
                            BatchFilesView, BatchDocumentsView,ReturnBatchListView,BatchUpdateView,
                            BatchSearchedListView,accept_batch,reject_batch,add_file_to_batch)
from app.view.document import DocumentDeleteView, DocumentView, UploadedDocumentsList, create_document
from app.view.document_type import DocumentTypeCreate, DocumentTypeList, DocumentTypeUpdate

from app.view.file_type import FileTypeCreate, FileTypeList,FileTypeUpdateView,FileTypeDeleteView
from app.view.scanner import upload_documents_to_file, ScannerTableView, delete_document
from app.view.transcribe import get_files_from_storage, update_document_file_detail, TranscribeFiles
from app.view.user import profile,admin_check_user,activate_deactivate
from .view.report import report, send_report_message, get_messages, mark_as_resolved, delete_document_type
from app.view.inspection import  inspect, receive, ReceiveBatch, OpenBatchFiles, DessembleFiles, DessemblerDocuments
from app.view.qa import  QaFileList, open_file_for_qa
from app.view.validate import ValidateFileList

from app.view.states_methods import update_state_batch,update_state_file
from app.view.stages_methods import update_stage_file
from app.view.file_history import (get_file_history,get_each_user_history,
    get_loggedin_user_history,user_specific_file_history,file_details,assign_file)
from app.view.escalations import RejectedDocumentFileList

from app.view.file_system import open_directory, view_directories
from app.view.stock import create_stock, StockListView, StockUpdateView, StockDeleteView, view_admin


# file manager imports
from .filemanager import (BrowserView, DetailView, UploadView,
                               UploadFileView, DirectoryCreateView, RenameView,
                               DeleteView)
from rest_framework.authtoken.views import obtain_auth_token
from .view.indexes import check_docs

from app.view.elderlyapp import mark_as_dead,guardian_create,dispath_to_one,DispatchListView
handler404 = 'app.views.handler404'
handler500 = 'app.views.handler500'

from django.contrib.auth.decorators import login_required,permission_required
from app.process_tasks import process_task
urlpatterns = [
    path('', login_required(permission_required(perm='is_superuser',raise_exception=True)(report)), name='home'),

    # batches
    #path('batches/admin', BatchListView.as_view(), name='batch_admin_index'),
    path('batches/<str:batch_type>/',login_required(permission_required(perm='app.view_batch',raise_exception=True)(BatchListView.as_view())),name='batch_type_index'),
    path('accept/batch/<int:pk>/',login_required(permission_required(perm='app.view_batch',raise_exception=True)(accept_batch)),name='accept_batch'),
    path('reject/batch/<int:pk>/', login_required(permission_required(perm='app.view_batch',raise_exception=True)(reject_batch)),name='reject_batch'),
    path('add/files/to/batch/<int:pk>',login_required(permission_required(perm='app.view_batch',raise_exception=True)(add_file_to_batch)), name='add_files_to_batch'),
    path('return/batches/', login_required(permission_required(perm='app.view_batch',raise_exception=True)(ReturnBatchListView.as_view())), name='batch_return'),
    path('create_batch/<str:batch_type>/',login_required(permission_required(perm='app.view_batch',raise_exception=True)(create_batch)),name='batch_create'),
    path('search_batch',login_required(permission_required(perm='app.view_batch',raise_exception=True)(search_batch)),name='batch_search'),
    path('search_batch/<int:pk>/', login_required(permission_required(perm='app.view_batch',raise_exception=True)(BatchSearchedListView.as_view())), name='batch_search_retrieve'),
    path('delete_batch/<int:pk>/', login_required(permission_required(perm='app.can_register_batch',raise_exception=True)(BatchDeleteView.as_view())), name='batch_delete'),
    path('edit_batch/<int:pk>/', login_required(permission_required(perm='app.can_register_batch',raise_exception=True)(BatchUpdateView.as_view())), name='batch_edit'),
    path('view/batch/<int:pk>/files/', login_required(permission_required(perm='app.view_batch',raise_exception=True)(BatchFilesView.as_view())), name='batch_files' ),##
    path('view/batch/<str:file_reference>/documents/', login_required(permission_required(perm='app.view_batch',raise_exception=True)(BatchDocumentsView.as_view())), name='batch_documents'),

    # file type urls
    path('create/file_type/', FileTypeCreate.as_view(), name='create_file_type'),
    path('list_files_types', FileTypeList.as_view(), name='list_file_types'),
    path('edit_file_type/<pk>/update/', FileTypeUpdateView.as_view(), name='edit_file_type'),
    path('delete_file_type/<pk>/delete/', FileTypeDeleteView.as_view(), name='delete_file_type'),


    # physical file urls
    path('batch/<int:batch_id>/files', FilesView.as_view(), name='files.view'),
    path('batch/<int:batch_id>/create/file/', DocumentFileCreate.as_view(), name='create_document_file'),
    path('list/document/files', DocumentFileList.as_view(), name='list_document_files'),
    path('list/document/files/qa', DocumentFileListQA.as_view(), name='list_document_files_qa'),
    path('list/transcribe/files', TranscribeFiles.as_view(), name='list_transcribe_files'),
    path('list_of_escalated_document_files', RejectedDocumentFileList.as_view(), name='rejected_list_document_files'),
    path('delete_file/<pk>/', FileDeleteView.as_view(), name='file_delete'),
    path('delete_edit/<pk>/', FileUpdateView.as_view(), name='file_edit'),
    path('search_file/', file_search, name='file_search'),
    path('file_search/<int:pk>', DocumentFileSearchedList.as_view(), name='file_search_retrieve'),
    path('request_file/', RequestFile.as_view(), name='file_request'),

    # Document Types
    path('create/document/type', DocumentTypeCreate.as_view(), name='create_document_type'),
    path('view/document/types', DocumentTypeList.as_view(), name='list_document_types'),
path('edit_document_type/<pk>/update/', DocumentTypeUpdate.as_view(), name='edit_document_type'),
    path('document/type/delete/<str:id>',delete_document_type, name='delete_doc_type'),

    # document upload and viewing
    path('file/<file_ref_no>/documents', DocumentView.as_view(), name='document.view'),
    path('file/<file_ref_no>/create_document/', create_document, name='document.create'),
    path('uploaded_documents', UploadedDocumentsList.as_view(), name='uploaded_documents'),
    path('files/upload/select',ScannerTableView.as_view(), name='get_file_to_upload_documents'),
    path('upload/to/file/<int:pk>',upload_documents_to_file, name='upload_document'),
    path('delete/uploaded/pdf/<int:id>',delete_document, name='delete_pdf'),
    path('delete_document/<pk>/', DocumentDeleteView.as_view(), name='document_delete'),


    # transcribe urls

    path('view_docs_in_file/<str:file_reference>', DocumentTranscribe.as_view(), name='view_docs_in_file'),
    path('transcription/lab/<int:doc_id>/<str:file_type>',get_document_and_document_type, name='transcription_lab'),
    path('update_doc_content/<int:doc_id>', update_document_content, name='update_doc_content' ),
    path('validate/doc/content/<int:doc_id>', validate_document_content, name='validate_doc_content'),

    path('file/document/storage/<file_reference>', get_files_from_storage, name='get_files_from_storage'),
    path('update/document/<int:document>',update_document_file_detail, name='update_document_file_detail'),

    # Auth
    path('all_users', UserListView.as_view(), name='users.index'),
    path('users/<int:user_type>', UserListView.as_view(), name='users.index.t'),
    path('loggedin/users/', LoggedInUserListView.as_view(), name='users.logged_in'),
    path('users/create/', user_create, name='users.create'),
    path('users/<int:pk>/', UserDetailView.as_view(), name='users.detail'),
    path('users/update/<int:pk>/', UserUpdateView.as_view(), name='user.update'),
    path('change_password/<username>', change_password, name='user.changepass'),
    path('activate/deactivate/user/<int:pk>', activate_deactivate, name='users.activate_deactivate'),
    path('user/delete/<int:pk>/', UserDeleteView.as_view(), name='user.delete'),
    # groups
    path('roles/', GroupListView.as_view(), name='groups.index'),
    path('roles/create/', add_group, name='roles.create'),
    path('roles/update/<int:pk>/', GroupUpdateView.as_view(), name='groups.update'),
    #path('roles/update/<int:pk>/', GroupD.as_view(), name='groups.update'),
    #

    path('login/', Login.as_view(), name='login'),

    path('logout/', LogoutView.as_view(), name='logout'),

    path('accept_file/<pk>/', change_file_status_to_accept, name='change_file_status_to_accept'),
    re_path(r'^accept_file/ajax/(?P<pk>\w+)/$', change_file_status_to_accept_ajax, name='change_file_status_to_accept_ajax'),
    path('reject_file/<pk>/', change_file_status_to_reject, name='change_file_status_to_reject'),
    path('accept_document/<pk>/', change_document_status_to_accept, name='change_document_status_to_accept'),
    path('reject_document/<pk>/', change_document_status_to_reject, name='change_document_status_to_reject'),
    path('return_rectified_file/<pk>/', return_rectified_file, name='return_rectified_file'),

    path('registry_submit_batch/<int:batch_id>/', registry_submit_to_receiver, name='registry_submit_batch'),
    path('receiver_submit_batch/<int:batch_id>/', receiver_batch_submit, name='receiver_submit_batch'),
#assembler
    path('inspect/docs/<int:pk>', inspect_docs, name='inspect_docs'),
    path('select_file/<pk>/', select_file, name='select_file'),
    path('scan/<file_ref>/', start_scanning, name='start_scan'),
    path('qa/<file_ref>/', start_qa, name='start_qa'),
    path('validate/<file_ref>/', start_validate, name='start_validate'),


    path('profile/', profile, name='profile'),
    path('reset_default_password', reset_default_password, name='reset_default_password'),
    path('check_user<int:pk>', admin_check_user, name='admin_check_user'),
    # api endpoints
    # path('api/v1/',ApiViewSet.as_view(), name='api'),

    # run file and document inspection
    # re_path(r'^inspect/file/$', inspect, name='inspect'),
    # re_path(r'^inspect/file/(?P<id>\w+)/$', inspect, name='inspect'),
    # re_path(r'^receive/batch/$', receive, name='receive'),
    # re_path(r'^receive/batch/(?P<id>\w+)/$', receive, name='receive'),

    re_path(r'^inspect/file/$', DessembleFiles.as_view(), name='inspect'),
    re_path(r'^inspect/file/(?P<id>\w+)/$', DessemblerDocuments.as_view(), name='inspect'),
    re_path(r'^receive/batch/$', ReceiveBatch.as_view(), name='receive'),
    re_path(r'^receive/batch/(?P<id>\w+)/$', OpenBatchFiles.as_view(), name='receive'),

    #quality assuarance link
    path('quality/files/', QaFileList.as_view(), name='quality'),
    path('validate/document/files', ValidateFileList.as_view(), name='validation'),


    #
    #stage and states urls
    #
    path('update_batch_state/<pk>/<action>/', update_state_batch,name='update_state_batch'),
    path('update_file_state/<pk>/<action>/', update_state_file,name='update_state_file'),


    #stage url

    path('update_file_stage/<pk>/<action>/', update_stage_file,name='update_stage_file'),

    #get file history

    path('file_history/<pk>/',get_file_history,name='file_history'),

    #get logged user history
    path('my/history/',get_loggedin_user_history,name='logged_in_user_history'),
    #user specific file history
    path('history_for_file/<pk>',user_specific_file_history,name='user_specific_file_history'),

#get  user history
    path('user/file_history/<pk>',get_each_user_history,name='get_user_history'),


#get escalated files
    path('user/escalated/files',RejectedDocumentFileList.as_view(), name='my_escalated_files'),

    #file details

    path('file/details/<pk>',file_details, name='file_details'),

#file details

    path('assign_file/<pk>',assign_file, name='file_assign'),

    # open files for qa
    path('file/qa/open/<pk>', open_file_for_qa, name='open_qa_file'),

    #send a report message
    path('send/report/',send_report_message, name='chat'),
    path('get/report/',get_messages, name='messages'),
    path('mark/resolved/<int:id>', mark_as_resolved, name='resolve'),
    path('complete/files/',CompleteFileList.as_view(),name='complete'),
    path('complete/files/<int:id>',file_internals,name='completeopen'),



    # filemanager urls
    url(r'^filemanager/$', BrowserView.as_view(), name='browser'),
    url(r'^detail/$', DetailView.as_view(), name='detail'),
    url(r'^upload/$', UploadView.as_view(), name='upload'),
    url(r'^upload/file/$', csrf_exempt(UploadFileView.as_view()), name='upload-file'),
    url(r'^create/directory/$', DirectoryCreateView.as_view(), name='create-directory'),
    url(r'^rename/$', RenameView.as_view(), name='rename'),
    url(r'^delete/$', DeleteView.as_view(), name='delete'),

    # second file manager
    path('storage/files',view_directories, name='storage_home'),
    path('storage/files/directory/<str:name>', open_directory, name='open_directory'),

    #stock taking urls
    path('stock/file', StockListView.as_view(),name='stock_index'),
    path('stock/create', create_stock, name='stock_create'),
    path('stock/update/<int:pk>/',StockUpdateView.as_view(), name='stock_update'),
    path('stock/delete/<int:pk>/', StockDeleteView.as_view(), name='stock_delete'),
    path('stock/reports',view_admin, name='stock_report'),



    #Scanner api
    path('api/token/', obtain_auth_token, name='obtain-token'),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^rest-auth/', include('rest_auth.urls')),
    path('api-v1/', include('api.urls')),

    #path('check_docs',check_docs),
    path('set_max_assignable_files/',set_max_assignable,name='set_max_assignable'),

    # bulk update
    path('unassign', unassign_files_from_users, name='unassign'),
    path('open', open_state_of_files, name='open'),
    path('close', close_state_of_files, name='close'),

    # statistics
    path('stats',statistics,name='stats'),

    # manual pdf splitting
    path('manual/split/<int:file_pk>', split_pdfs, name='manual_split'),
    path('process_task_just_dummy8989',process_task),

    path('mark_as_dead/<int:pk>', mark_as_dead, name='mark_as_dead'),
    path('add_guardian/<int:elderly_id>', guardian_create, name='create_guardian'),
    path('dispatch_moneyto/<int:pk>', dispath_to_one, name='dispatch_to_one'),
    path('disbursment/', DispatchListView.as_view(), name='disbursment'),




]

