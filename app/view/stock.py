from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from django.urls import reverse, reverse_lazy
from django.utils import timezone
import pytz
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from django_filters.views import FilterView
from django_tables2 import SingleTableMixin, RequestConfig
from datetime import datetime, timedelta, time
from app.filters import StockFilter
from app.forms import StockForm
from app.models import Stock
from app.tables import StockTable
from django.contrib.auth.models import User, Permission
from operator import itemgetter


class StockListView(LoginRequiredMixin, SingleTableMixin, FilterView):
    table_class = StockTable
    template_name = 'stock/index.html'
    filterset_class = StockFilter

    def get_queryset(self):
        queryset = Stock.objects.all()
        self.table = StockTable(queryset)
        self.filter = StockFilter(self.request.GET,
                                  Stock.objects.all())
        self.table = StockTable(self.filter.qs)
        RequestConfig(self.request, paginate={'per_page': 10}).configure(self.table)

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context['table'] = self.table
        context['filter'] = self.filter
        return context


class StockUpdateView(LoginRequiredMixin, SuccessMessageMixin, UserPassesTestMixin, UpdateView):
    model = Stock
    fields = ['file_number', 'name', 'nationality', 'cross_reference', 'file_category', 'date_last_correspondence',
              'date_first_correspondence', 'location_of_file', 'comment']
    template_name = 'stock/create.html'
    success_url = reverse_lazy('stock_index')
    success_message = 'Record updated successfully'

    def form_valid(self, form):
        return super().form_valid(form)

    def test_func(self):
        stock = self.get_object()
        if stock:
            return True
        return False


@login_required
def create_stock(request):
    if request.method == 'POST':
        form = StockForm(data=request.POST)

        if form.is_valid():
            try:
                stock = Stock(file_number=form.cleaned_data.get('file_number'),
                              comment=form.cleaned_data.get('comment'),
                              name=form.cleaned_data.get('name'),
                              nationality=form.cleaned_data.get('nationality'),
                              cross_reference=form.cleaned_data.get('cross_reference'),
                              file_category=form.cleaned_data.get('file_category'),
                              date_last_correspondence=form.cleaned_data.get('date_last_correspondence'),
                              date_first_correspondence=form.cleaned_data.get(
                                  'date_first_correspondence'),
                              location_of_file=form.cleaned_data.get('location_of_file'),
                              created_by=request.user)
                stock.save()


                # sql_logger("Create a new stock", stock.save().__str__())
                messages.success(request, f" File Recorded successfully")

                return redirect('stock_index')

            except AttributeError as e:

                messages.error(request, ' something wrong happened while adding batch')
                return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
    else:
        form = StockForm()
    return render(request, 'stock/create.html', {'form': form})


class StockDeleteView(LoginRequiredMixin, SuccessMessageMixin, UserPassesTestMixin, DeleteView):
    model = Stock

    success_url = reverse_lazy('stock_index')
    success_message = 'Record Deleted Successfully'
    template_name = 'stock/delete_confirm.html'

    def test_func(self):
        stock = self.get_object()
        if stock:
            return True
        return False


@login_required
def view_admin(request):
    today = timezone.localdate()
    total_files = Stock.objects.all().count()
    perm_can_manage_stock = Permission.objects.get(codename="can_manage_stock")
    inventory_users = User.objects.filter(groups__permissions=perm_can_manage_stock)
    total_users = inventory_users.count()
    user_data = []
    for user in inventory_users:
        user_total_files = user.stock_set.count()
        total_files_today = user.stock_set.filter(created_at__gte=today).count()
        user_data.append({
            "username": user.username,
            "total_files": user_total_files,
            "files_today": total_files_today
        })

    files_today = Stock.objects.filter(created_at__gte=today).count()
    files_today_table = StockTable(Stock.objects.filter(created_at__gte=today)[:20])
    commented_files = Stock.objects.exclude(comment__isnull=True).exclude(comment__exact='').count()
    commented_table = StockTable(Stock.objects.exclude(comment__isnull=True).exclude(comment__exact='')[:50])
    user_table = StockTable(User.objects.filter(groups__permissions=perm_can_manage_stock))

    files_per_user = Stock.objects.all()
    context = {
        "files": total_files,
        "total_users": total_users,
        "files_today": files_today,
        "file_table": files_today_table,
        "commented": commented_files,
        "commented_table": commented_table,
        "user_data_table": sorted(user_data, key=itemgetter('files_today'), reverse=True)

    }
    return render(request, "stock/report.html", context)


def open_files(request):
    # return a table
    context = {
        "num": 2
    }
    return render(request, "stock/files.html", context)
