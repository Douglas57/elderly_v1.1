from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q
from django.shortcuts import redirect, reverse

from django.contrib import messages

from django.contrib.contenttypes.models import ContentType
from django.shortcuts import render, redirect
import itertools

from django_filters.views import FilterView
from django_tables2 import SingleTableMixin, RequestConfig

from app.filters import DocumentFilter, BatchFilter, DocumentFileFilter
from app.models import DocumentFileDetail, DocumentFile, Batch, Modification, STAGES, STATES
from app.tables import ReceiverTable, ReceiverFiles, AssemblerFiles, AssemblerDocuments, DocumentTable

def check_docs(request):

    return render(request,'check_docs.html',{'documents':[9090,898980,87987,7878]})

