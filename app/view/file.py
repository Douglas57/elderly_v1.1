import urllib
from django.conf import settings
import os
from json2html import *
import json
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.auth.models import Permission
from django.shortcuts import redirect, render
from django.db.models import Q
from django.urls import reverse_lazy, reverse

from django.utils import timezone
from django.views.generic import ListView, CreateView
from django_filters.views import FilterView
from django_tables2 import SingleTableMixin, RequestConfig
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.views.generic import DeleteView, UpdateView
from app.filters import DocumentFileFilter
from app.models import DocumentFile, STAGES, STATES, DocumentFileDetail, Filer
from app.tables import DocumentFileTable, BatchFileTable, CompleteFiles, DocumentFileQATable


class DocumentFileCreate(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    permission_required = 'app.add_documentfile'
    success_message = 'File created successfully'
    model = DocumentFile
    template_name = 'file/create.html'
    fields = ['file_reference', 'file_type', 'file_barcode']

    def form_valid(self, form):
        form.instance.file_created_by = self.request.user
        form.instance.assigned_to = self.request.user
        form.instance.batch_id = self.kwargs['batch_id']
        # file=form.save()

        return super().form_valid(form)

    def get_success_url(self):
        return reverse('batch_files', kwargs={'pk': self.kwargs['batch_id']})


class FilesView(LoginRequiredMixin, SingleTableMixin, FilterView):
    template_name = 'batch/filetable.html'
    table_class = BatchFileTable
    filterset_class = DocumentFileFilter

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context['table'] = self.table
        context['filter'] = self.filter
        context['batch_id'] = int(self.kwargs['batch_id'])
        return context

    def get_queryset(self):
        queryset = DocumentFile.objects.none()
        if self.request.user.is_superuser:
            queryset = DocumentFile.objects.filter(flagged=False)
        elif self.request.user.has_perm('app.can_register_batch'):

            queryset = DocumentFile.objects.filter(
                batch_id=int(self.kwargs['batch_id']),
                stage=STAGES[0],
                flagged=False)

        elif self.request.user.has_perm('app.can_receive_file'):

            q1 = DocumentFile.objects.filter(stage=STAGES[1],
                                             batch_id=int(self.kwargs['batch_id']),
                                             flagged=False,
                                             assigned_to=self.request.user)
            q2 = DocumentFile.objects.filter(stage=STAGES[1],
                                             batch_id=int(self.kwargs['batch_id']),
                                             flagged=False,
                                             assigned_to=None)
            queryset = q1.union(q2)

        self.table = BatchFileTable(queryset)
        self.filter = DocumentFileFilter(self.request.GET,
                                         queryset)
        self.table = BatchFileTable(self.filter.queryset)
        RequestConfig(self.request, paginate={'per_page': 10}).configure(self.table)


# TODO check states (Tuple Out Of Index)
class DocumentFileList(LoginRequiredMixin, SingleTableMixin, FilterView):


    table_class = DocumentFileTable
    template_name = 'view_document_files.html'
    filterset_class = DocumentFileFilter

    def get_queryset(self):

        if self.request.user.is_superuser:
            queryset = DocumentFile.objects.select_related('assigned_to').filter(flagged=False).order_by('created_on')
        elif self.request.user.has_perm('app.can_create_batch'):
            queryset = DocumentFile.objects.select_related('assigned_to').filter(stage=STAGES[0], flagged=False, assigned_to=self.request.user)
        elif self.request.user.has_perm('app.can_receive_file'):
            queryset = DocumentFile.objects.select_related('assigned_to').filter(flagged=False)
        elif self.request.user.has_perm('app.can_disassemble_file'):
            queryset = DocumentFile.objects.select_related('assigned_to').filter(stage=STAGES[2], flagged=False, assigned_to=self.request.user)
        elif self.request.user.has_perm('app.can_scan_file'):

            queryset = DocumentFile.objects.select_related('assigned_to').filter(stage=STAGES[3],
                                                   flagged=False,
                                                   assigned_to=self.request.user)



        elif self.request.user.has_perm('app.can_transcribe_file'):

            queryset = DocumentFile.objects.select_related('assigned_to').filter(stage=STAGES[4], flagged=False, assigned_to=self.request.user)


        elif self.request.user.has_perm('app.can_qa_file'):
            queryset = DocumentFile.objects.select_related('assigned_to').filter(stage=STAGES[5], flagged=False, assigned_to=self.request.user)

        elif self.request.user.has_perm('app.can_validate_file'):
            queryset = DocumentFile.objects.select_related('assigned_to').filter(stage=STAGES[6], flagged=False,
                                                   assigned_to=self.request.user)
        else:
            queryset = DocumentFile.objects.none()

        self.table = DocumentFileTable(queryset)
        self.filter = DocumentFileFilter(self.request.GET,
                                         queryset)
        self.table = DocumentFileTable(self.filter.qs)
        RequestConfig(self.request, paginate={'per_page': 10}).configure(self.table)

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context['table'] = self.table
        context['filter'] = self.filter
        return context


class DocumentFileListQA(LoginRequiredMixin, SingleTableMixin, FilterView):

    table_class = DocumentFileQATable
    template_name = 'view_document_files.html'
    filterset_class = DocumentFileFilter

    def get_queryset(self):

        if self.request.user.is_superuser:
            queryset = DocumentFile.objects.filter(flagged=False)
        elif self.request.user.has_perm('app.can_create_batch'):
            queryset = DocumentFile.objects.filter(stage=STAGES[0], flagged=False, assigned_to=self.request.user)
        elif self.request.user.has_perm('app.can_receive_file'):
            queryset = DocumentFile.objects.filter(flagged=False)
        elif self.request.user.has_perm('app.can_disassemble_file'):
            queryset = DocumentFile.objects.filter(stage=STAGES[2], flagged=False, assigned_to=self.request.user)
        elif self.request.user.has_perm('app.can_scan_file'):

            queryset = DocumentFile.objects.filter(stage=STAGES[3],
                                                   flagged=False,
                                                   assigned_to=self.request.user)



        elif self.request.user.has_perm('app.can_transcribe_file'):

            queryset = DocumentFile.objects.filter(stage=STAGES[4], flagged=False, assigned_to=self.request.user)


        elif self.request.user.has_perm('app.can_qa_file'):
            queryset = DocumentFile.objects.filter(stage=STAGES[5], flagged=False, assigned_to=self.request.user)

        elif self.request.user.has_perm('app.can_validate_file'):
            queryset = DocumentFile.objects.filter(stage=STAGES[6], flagged=False,
                                                   assigned_to=self.request.user)
        else:
            queryset = DocumentFile.objects.none()

        self.table = DocumentFileQATable(queryset)
        self.filter = DocumentFileFilter(self.request.GET,
                                         queryset)
        self.table = DocumentFileQATable(self.filter.qs)
        RequestConfig(self.request, paginate={'per_page': 10}).configure(self.table)

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context['table'] = self.table
        context['filter'] = self.filter
        return context




class RejectedDocumentFileList(LoginRequiredMixin, SingleTableMixin, FilterView):
    table_class = DocumentFileTable
    template_name = 'file/rejected_file_documents_list.html'

    def get_queryset(self):
        RequestConfig(self.request, paginate={'per_page': 10}).configure(self.table)
        if self.request.user.is_superuser:
            return DocumentFile.objects.DocumentFile.objects.filter(
                flagged=True)
        elif self.request.user.has_perm('app.can_create_batch'):
            return DocumentFile.objects.filter(flagged=True).filter(
                stage=STAGES[0],
                assigned_to=self.request.user)
        elif self.request.user.has_perm('app.can_receive_file'):
            return DocumentFile.objects.filter(flagged=True).filter(
                stage=STAGES[1],
                assigned_to=self.request.user)
        elif self.request.user.has_perm('app.can_disassemble_file'):
            return DocumentFile.objects.filter(flagged=True).filter(
                stage=STAGES[2],
                assigned_to=self.request.user)
        elif self.request.user.has_perm('app.can_scan_file'):

            return DocumentFile.objects.filter(flagged=True).filter(
                stage=STAGES[3],
                assigned_to=self.request.user)


        elif self.request.user.has_perm('app.can_transcribe_file'):

            return DocumentFile.objects.filter(flagged=True).filter(
                stage=STAGES[4],
                assigned_to=self.request.user)

        elif self.request.user.has_perm('app.can_qa_file'):
            return DocumentFile.objects.filter(flagged=True).filter(
                stage=STAGES[5],
                assigned_to=self.request.user)

        elif self.request.user.has_perm('app.can_validate_file'):
            return DocumentFile.objects.filter(flagged=True).filter(
                stage=STAGES[6],
                assigned_to=self.request.user)
        else:
            return DocumentFile.objects.none()

    filterset_class = DocumentFileFilter


class FileUpdateView(LoginRequiredMixin, SuccessMessageMixin, UserPassesTestMixin, UpdateView):
    model = DocumentFile
    fields = ['file_type', 'file_reference', 'file_barcode']
    template_name = 'file/create.html'
    success_message = 'File updated successfully'


    def form_valid(self, form):
        form.instance.file_created_by = self.request.user
        return super().form_valid(form)
    def get_success_url(self):
        file = self.get_object()
        return reverse('batch_files',kwargs={'pk':file.original_batch_id})

    def test_func(self):

        if self.request.user.has_perm('app.can_register_batch'):
            return True
        return False


class FileDeleteView(LoginRequiredMixin, SuccessMessageMixin, UserPassesTestMixin, DeleteView):
    model = DocumentFile

    success_message = 'File Deleted Successfully'
    template_name = 'file/delete_confirm.html'
    def get_success_url(self):
        file = self.get_object()
        return reverse('batch_files',kwargs={'pk':file.original_batch_id})
    def test_func(self):
        if self.request.user.has_perm('app.can_register_batch'):
            return True
        return False


# open a complete file
# see all its contents


class CompleteFileList(LoginRequiredMixin, SingleTableMixin, FilterView):
    permission_required = 'app.view_documentfile'
    table_class = CompleteFiles
    template_name = 'view_document_files.html'
    filterset_class = DocumentFileFilter

    def get_queryset(self):
        queryset = DocumentFile.objects.filter(Q(stage__in=[STAGES[6], STAGES[7]]))
        self.table = CompleteFiles(queryset)
        self.filter = DocumentFileFilter(self.request.GET,
                                         queryset)
        self.table = CompleteFiles(self.filter.qs)
        RequestConfig(self.request, paginate={'per_page': 10}).configure(self.table)

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context['table'] = self.table
        context['filter'] = self.filter
        return context


def file_internals(request, id):
    documents = DocumentFileDetail.objects.filter(file_reference=id)
    pdf = Filer.objects.filter(file_reference_id=id).latest('id')
    large_table = []
    for document in documents:
        content = document.document_content
        if content is not None:
            table_data = json2html.convert(json=content, table_attributes="id=\"info-table\" class=\"table table-bordered "
                                                                          "table-hover\"")
            large_table.append(table_data)

    document_file_path = ''
    if pdf is not None:
        url = str(pdf.filepond)
        document_file_path = urllib.parse.quote(url)



    return render(request, 'file/pages.html', {'table_data': large_table, 'document_file_path': document_file_path})


def file_search(request):
    if request.method == 'POST':

       file_barcode=request.POST['file_barcode']
       file_stage=''
       if request.user.has_perm('app.can_register_batch'):
           file_stage=STAGES[0]
       elif request.user.has_perm('app.can_receive_file'):
           file_stage = STAGES[1]
       elif request.user.has_perm('app.can_disassemble_file'):
           file_stage = STAGES[2]
       elif request.user.has_perm('app.can_scan_file'):
           file_stage = STAGES[3]
       elif request.user.has_perm('app.can_transcribe_file'):
           file_stage = STAGES[4]
       elif request.user.has_perm('app.can_qa_file'):
           file_stage = STAGES[5]
       elif request.user.has_perm('app.can_validate_file'):
           file_stage = STAGES[6]
       try:
           file = DocumentFile.objects.get(file_barcode=file_barcode,
                                        stage = file_stage,
                                         flagged = False,

                                             assigned_to_id = None
                                             )

           if file.state == STATES[1]:
               file.open(request.user)
           file.save()

           return redirect(reverse('list_document_files'))

       except Exception as e:
           messages.error(request, e)
           return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

    else:
        page_title = ''
        if request.user.has_perm('app.can_disassemble_file'):
            page_title = 'Assemble/Disassemble'
        if request.user.has_perm('app.can_scan_file'):
            page_title = 'Scan'
        if request.user.has_perm('app.can_qa_file'):
            page_title = 'Quality Assurance'
        if request.user.has_perm('app.can_validate_file'):
            page_title = 'Validation'

        data = {
            'title1': 'File',
            'title2': page_title,
            'post_url': 'file_search',
            'placeholder': 'File Barcode',
            'name_attribute': 'file_barcode'
        }
        return render(request, 'batch/search.html', data)


class DocumentFileSearchedList(LoginRequiredMixin, SingleTableMixin, FilterView):
    permission_required = 'app.view_documentfile'

    table_class = DocumentFileTable
    template_name = 'view_document_files.html'
    filterset_class = DocumentFileFilter

    def get_queryset(self):

        if self.request.user.has_perm('app.can_create_batch'):
            queryset = DocumentFile.objects.filter(stage=STAGES[0], flagged=False,
                                                   pk=self.kwargs.get('pk'),
                                                   assigned_to_id=None)
        elif self.request.user.has_perm('app.can_receive_file'):
            queryset = DocumentFile.objects.filter(stage=STAGES[1], flagged=False,
                                                   pk=self.kwargs.get('pk'),
                                                   assigned_to_id=None)
        elif self.request.user.has_perm('app.can_disassemble_file'):
            queryset = DocumentFile.objects.filter(stage=STAGES[2], flagged=False, pk=self.kwargs.get('pk'),
                                                   assigned_to_id=None)
        elif self.request.user.has_perm('app.can_scan_file'):
            queryset = DocumentFile.objects.filter(stage=STAGES[3], flagged=False, pk=self.kwargs.get('pk'),
                                                   assigned_to_id=None)
        elif self.request.user.has_perm('app.can_transcribe_file'):

            queryset = DocumentFile.objects.filter(stage=STAGES[4], flagged=False, pk=self.kwargs.get('pk'),
                                                   assigned_to_id=None)
        elif self.request.user.has_perm('app.can_qa_file'):
            queryset = DocumentFile.objects.filter(stage=STAGES[5], flagged=False, pk=self.kwargs.get('pk'),
                                                   assigned_to_id=None)


        elif self.request.user.has_perm('app.can_validate_file'):
            queryset = DocumentFile.objects.filter(stage=STAGES[6], flagged=False, pk=self.kwargs.get('pk'),
                                                   assigned_to_id=None)
        else:
            queryset = DocumentFile.objects.none()

        self.table = DocumentFileTable(queryset)
        self.filter = DocumentFileFilter(self.request.GET,
                                         queryset)
        self.table = DocumentFileTable(self.filter.qs)
        if queryset:
            file = queryset.first()
            file.assigned_to = self.request.user
            file.save()

        RequestConfig(self.request, paginate={'per_page': 10}).configure(self.table)

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context['table'] = self.table
        context['filter'] = self.filter
        return context


class RequestFile(LoginRequiredMixin, SingleTableMixin, FilterView):
    permission_required = 'app.view_documentfile'

    table_class = DocumentFileTable
    template_name = 'view_document_files.html'
    filterset_class = DocumentFileFilter

    def get_queryset(self):

        queryset = DocumentFile.objects.none()
        max_files_assignable=None
        try:
            module_dir = os.path.dirname(__file__)  # get current directory
            file_path = os.path.join(module_dir, 'max_files_assignable.txt')
            with open(file_path,"r") as max_file:
                     file_line =max_file.readline()

            json_data=json.loads(file_line)


            max_files_assignable=int(json_data['max_files_assignable'])

        except Exception as e:
            max_files_assignable=3



        if DocumentFile.objects.filter(assigned_to_id=self.request.user.pk).count() <= max_files_assignable:
            if self.request.user.has_perm('app.can_transcribe_file'):

                single_obj = DocumentFile.objects.filter(stage=STAGES[4], flagged=False, assigned_to=None).order_by('created_on').first()

                if not single_obj:

                    messages.add_message(self.request, messages.WARNING, "No files available for now")
                else:
                    queryset = DocumentFile.objects.filter(pk=single_obj.pk)

            else:
                queryset = DocumentFile.objects.none()
                messages.add_message(self.request, messages.ERROR, "you don't have permission to edit this file")
        else:
            queryset = DocumentFile.objects.none()
            messages.add_message(self.request, messages.ERROR, "You have reached maximum assignable files")
        self.table = DocumentFileTable(queryset)
        self.filter = DocumentFileFilter(self.request.GET,
                                         queryset)
        self.table = DocumentFileTable(self.filter.qs)
        RequestConfig(self.request, paginate={'per_page': 10}).configure(self.table)

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context['table'] = self.table
        context['filter'] = self.filter
        return context


@login_required
def inspect_docs(request, pk):
    documents = DocumentFileDetail.objects.filter(file_reference_id=pk)
    file =DocumentFile.objects.get(pk=pk)
    document_barcodes = []
    for doc in documents:
        document_barcodes.append(doc.document_barcode)

    return render(request, 'check_docs.html', {'document_barcodes': document_barcodes,
                                               'file_state':file.state,
                                               'file_pk':file.pk})

@login_required
def set_max_assignable(request):

    if request.method == 'POST':
        try:
            max_value=request.POST.get('max_value')

            module_dir = os.path.dirname(__file__)  # get current directory
            file_path = os.path.join(module_dir, 'max_files_assignable.txt')
            with open(file_path,"w") as f:
                json.dump({"max_files_assignable" : max_value},f)

            messages.success(request, "Settings updated")
        except Exception as e:
            messages.error(request, e)

        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
    else:
        max_files_assignable = None
        try:
            module_dir = os.path.dirname(__file__)  # get current directory
            file_path = os.path.join(module_dir, 'max_files_assignable.txt')
            with open(file_path, "r") as max_file:
                file_line = max_file.readline()

            json_data = json.loads(file_line)

            max_files_assignable = int(json_data['max_files_assignable'])

        except Exception as e:
            max_files_assignable = 0

        return render(request,'set_max_file.html',{'max_val':max_files_assignable})
