from app.models import STATES, STAGES,Batch,DocumentFile,DocumentFileDetail
from django.shortcuts import redirect, render
from django.urls import reverse, reverse_lazy
from django.http import HttpResponseRedirect
from django.contrib import messages

ACTIONS=['Open','Done','Continue_Editing','Close']

def update_state_batch(request, pk, action):
    """update the state of the batch"""
    batch= Batch.objects.get(pk=pk)
    user = request.user
    if batch:
        try:
            if action == ACTIONS[0]:

                batch.open(user=user)
                batch.save()
                messages.success(request, ' Batch status changed successfully')
                return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
            elif action == ACTIONS[1]:
                batch.done(user=user)
                batch.save()
                messages.success(request, ' Batch status changed successfully')
                return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
            elif action == ACTIONS[2]:
                batch.continue_editing(user=user)
                batch.save()
                messages.success(request, ' Batch status changed successfully')
                return redirect(reverse('files.view', kwargs={'batch_id': batch.id}))
            elif action == ACTIONS[3]:
                batch.close(user=user,comment='')
                batch.save()
                messages.success(request, ' Batch status changed successfully')
                return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
            else:
                messages.error(request, ' No action selected')
                return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

        except Exception as e:
            messages.error(request, e)
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

def update_state_file(request, pk, action):
    """update the state of the batch"""
    file= DocumentFile.objects.get(pk=pk)

    user = request.user
    if file:
        try:
            if action == ACTIONS[0]:

                file.open(user=user)
                file.save()
                if request.user.has_perm('app.can_receive_file') and not request.user.is_superuser:
                    messages.success(request, ' File status changed successfully')
                    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
                messages.success(request, ' File status changed successfully')
                return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

            elif action == ACTIONS[3]:
                file.close(user=user)
                file.save()
                messages.success(request, ' File status changed successfully')
                return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
            else:
                messages.error(request, ' No action selected')
                return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

        except Exception as e:
            messages.error(request, " Action denied, kindly ensure that all conditions are met")
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


