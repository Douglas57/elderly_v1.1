import time
import json
# from django.core.serializers import json
from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import get_object_or_404
from django_filters.views import FilterView
from django_tables2 import SingleTableMixin, RequestConfig
from app.tables import TranscribeTable
from app.filters import DocumentFileFilter
from PyPDF2 import PdfFileWriter, PdfFileReader
from django.core.files.storage import FileSystemStorage
import os
import time
import ast
from datetime import datetime
from pathlib import Path
import concurrent.futures
from app.sql_generator import sql_logger

from app.models import Filer, DocumentFile, DocumentFileDetail, DocumentType, STAGES
page_number = 0

def map_keys_to_value_digital(dict_list):
    selectors = []
    for item in dict_list:
        value = item['id']
        label = item['document_barcode']
        dict = {'value': value, 'label': label}
        selectors.append(dict)
    return selectors


def get_files_from_storage(request, file_reference):
    doc_int = DocumentFileDetail.objects.get(pk=file_reference).file_reference_id
    scanned_documents = Filer.objects.filter(file_reference=doc_int).values('filepond', 'file_reference')[:1]
    digital_documents = DocumentFileDetail.objects.filter(file_reference=doc_int).values()
    document_type = DocumentType.objects.all().values('document_name')
    file = DocumentFile.objects.get(pk=int(doc_int))
    domain = request.get_host()

    pages = []
    documents = []
    pdf = list(scanned_documents)
    doc_pdf = pdf[0]['filepond']
    pages.append(doc_pdf)
    directory = os.path.dirname(doc_pdf)
    try:
        search_dir = Path("media/" + directory + "/")
        for file in os.listdir(search_dir):
            print(file)
            base = os.path.basename(file)
            file_name = os.path.splitext(base)[0]

            if  file_name.isdigit():
                file_url = directory + "/" + file
                pages.append(file_url)

    except Exception as exc:
        print(exc)

    for page in pages:
        documents.append({'filepond': page, 'file_reference': scanned_documents[0]['file_reference']})

    context = {'scanned_documents': documents,
               'digital_documents': map_keys_to_value_digital(digital_documents), 'file': file, 'domain': domain,
               'document_type': list(document_type)}

    return render(request, 'transcribe_document.html', context=context)


def merge_pdfs(paths, output):
    pdf_writer = PdfFileWriter()

    for path in paths:
        pdf_reader = PdfFileReader(path)
        for page in range(pdf_reader.getNumPages()):
            # Add each page to the writer object
            pdf_writer.addPage(pdf_reader.getPage(page))

    # Write out the merged PDF
    with open(output, 'wb') as out:
        pdf_writer.write(out)


@csrf_exempt
def update_document_file_detail(request, document):
    # get the document
    # update the document_path, document_type
    document_path = ''
    start_time = time.time()

    if request.POST and document is not None:
        document = get_object_or_404(DocumentFileDetail, pk=document)
        if document is None:
            response = JsonResponse()
            response.status_code = 400
            return response
        pdfs = request.POST.get('related')

        pdfs = pdfs.split(",")

        if not '' in pdfs:
            if len(pdfs) > 0:
                # pdf_arrays = ast.literal_eval(pdfs)
                sanitized_arrays = []
                # # find each pdfs in the storage
                path = os.path.dirname(pdfs[0])
                for i in range(len(pdfs)):
                    new_path = "media/" + pdfs[i]
                    sanitized_arrays.append(new_path)

                # print(path)
                merged_pdfs = "media/" + path + "/" + "merge" + str(int(time.time()))
                merge_pdfs(sanitized_arrays, output=merged_pdfs + '.pdf')

                document_path = merged_pdfs[6:] + ".pdf"

        else:
            document_path = request.POST.get('document_path')


        document_type = request.POST.get('document_type')
        document_type_instance = get_object_or_404(DocumentType, pk=document_type)
        if document_type_instance is None:
            response = JsonResponse()
            response.status_code = 400
            return response
        document.document_type = document_type_instance
        document.document_file_path = document_path
        document.save()

        sql_logger('update document path')
        response = JsonResponse({'success': 'update was success'})
        response.status_code = 200
        return response
    else:
        response = JsonResponse({'error': 'update failed'})
        response.status_code = 500
        return response


class TranscribeFiles(SingleTableMixin, FilterView):
    table_class = TranscribeTable
    filterset_class = DocumentFileFilter
    model = DocumentFile
    template_name = "transcribe_list.html"

    def get_queryset(self):
        queryset = DocumentFile.objects.filter(stage=STAGES[4], flagged=False).order_by('-created_on')
        self.table = TranscribeTable(queryset)
        self.filter = DocumentFileFilter(self.request.GET,
                                         DocumentFile.objects.filter(stage=STAGES[4], flagged=False).order_by(
                                             '-created_on'))
        self.table = TranscribeTable(self.filter.qs)
        RequestConfig(self.request, paginate={'per_page': 10}).configure(self.table)

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context['table'] = self.table
        context['filter'] = self.filter
        return context
