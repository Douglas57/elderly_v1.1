from datetime import date

from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.contrib.auth.models import User
from django.db.models import Q
from django.contrib.auth.decorators import login_required
from app.models import *
from app.signals import file_uploaded

@login_required
def report(request):

    today = date.today()
    registry_todays_files = Modification.objects.filter(created_at__day=today.day, modified_to_stage=STAGES[1]).count()
    reception_todays_files=Modification.objects.filter(Q(created_at__day=today.day,modified_to_stage=STAGES[0])
                                                        | Q(created_at__day=today.day,modified_to_stage=STAGES[2])).count()
    assembler_todays_files = Modification.objects.filter( Q(created_at__day=today.day, modified_to_stage=STAGES[1])
                                                           | Q(created_at__day=today.day,modified_to_stage=STAGES[3])).count()
    scanner_todays_files = Modification.objects.filter(Q(created_at__day=today.day, modified_to_stage=STAGES[2])
                                                          | Q(created_at__day=today.day,
                                                              modified_to_stage=STAGES[4])).count()
    transcriber_todays_files = Modification.objects.filter(Q(created_at__day=today.day, modified_to_stage=STAGES[3])
                                                        | Q(created_at__day=today.day,
                                                            modified_to_stage=STAGES[5])).count()
    qa_todays_files = Modification.objects.filter(Q(created_at__day=today.day, modified_to_stage=STAGES[4])
                                                        | Q(created_at__day=today.day,
                                                            modified_to_stage=STAGES[6])).count()
    validator_todays_files = Modification.objects.filter(Q(created_at__day=today.day, modified_to_stage=STAGES[5])
                                                        | Q(created_at__day=today.day,
                                                            modified_to_stage=STAGES[7])).count()
    MAX = 5
    # get all documents
    documents = DocumentFileDetail.objects.all()

    # get all files
    files = DocumentFile.objects.all()
    # get all document types
    document_types = DocumentType.objects.all()
    # get all file types
    file_types = DocumentFileType.objects.all()
    # get all users
    users = User.objects.all()

    file = DocumentFile.objects.first()

    reject_file = DocumentFile.objects.filter(
        flagged=True).order_by('created_on')

    registry = DocumentFile.objects.filter(stage=STAGES[0]).filter(flagged=False).order_by('-created_on')

    reception = DocumentFile.objects.filter(stage=STAGES[1]).filter(flagged=False).order_by('-created_on')

    disassembly = DocumentFile.objects.filter(stage=STAGES[2]).filter(flagged=False).order_by('-created_on')
    transcription = DocumentFile.objects.filter(stage=STAGES[4]).filter(flagged=False).order_by('-created_on')
    scanning = DocumentFile.objects.filter(stage=STAGES[3]).filter(flagged=False).order_by('-created_on')
    qa = DocumentFile.objects.filter(stage=STAGES[5]).filter(flagged=False).order_by('-created_on')
    elderly=User.objects.filter(groups__name__in=['Elderly'], is_superuser=False)
    guardian = User.objects.filter(groups__name__in=['Guardian'], is_superuser=False)
    dead = User.objects.filter(groups__name__in=['Dead'], is_superuser=False)
    admins=User.objects.filter(is_superuser=True)

    context = {
        "documents": documents,
        "files": files,
        "document_types": document_types,
        "file_types": file_types,
        "users": users,
        "registry": registry,
        "reception": reception,
        "qa": qa,
        "scanning": scanning,
        "transcription": transcription,
        "disassembly": disassembly,

        "rejected_file": reject_file,
        "registry_todays_files":registry_todays_files,
        "reception_todays_files": reception_todays_files,
        "assembler_todays_files": assembler_todays_files,
        "scanner_todays_files": scanner_todays_files,
        "transcriber_todays_files": transcriber_todays_files,
        "qa_todays_files": qa_todays_files,
        "validator_todays_files": validator_todays_files,
        'admins':admins,
        'elderly':elderly,
        'guardians':guardian,
        'dead':dead
    }

    return render(request, "home.html", context)



def send_report_message(request):
    report = request.GET.get('reasons')
    Notification.objects.create(comment=report, created_by=request.user)
    # notification.save()
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


def get_messages(request):
    messages = Notification.objects.filter(file_id=None).filter(resolved=False).order_by('-created_at')[:20]
    return render(request, 'messages.html', {'messages': messages})


def mark_as_resolved(request, id):
    message = Notification.objects.get(pk=id)
    message.resolved_by = request.user
    message.resolved = True
    message.save()
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


def delete_document_type(request, id):
    document_type = DocumentType.objects.filter(pk=id)
    document_type.delete()
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
