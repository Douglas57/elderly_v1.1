from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponseRedirect
from django.contrib import messages
from django.contrib.auth.models import User, Permission
from app.models import DocumentFile, Modification, STAGES
from django.shortcuts import render
from app.tables import HistoryTable, SpecificFileUserHistoryTable
from django.db.models import Count
from django.contrib.auth.decorators import login_required


def get_file_history(request, pk):
    file = DocumentFile.objects.get(pk=pk)
    if file:
        history = file.modification_set.all()
    else:
        history = Modification.objects.none()
    return render(request, 'file/file_history.html', {'file': file, 'history': history})


def get_each_user_history(request, pk):
    user = User.objects.get(pk=pk)
    # table = HistoryTable(Modification.objects.filter(by=user))
    table = HistoryTable(Modification.objects.filter(by=user).order_by('file', '-created_at').distinct('file'))
    return render(request, 'user/user_hIstory.html', {'table': table})


@login_required
def get_loggedin_user_history(request):
    user = request.user

    if user.is_superuser:
        # table = HistoryTable(Modification.objects.filter(by=user))
        table = HistoryTable(Modification.objects.filter(by=user).order_by('file','-created_at').distinct('file'))

    else:
        table = HistoryTable(Modification.objects.filter(by=user).order_by('file','-created_at').distinct('file'))
    return render(request, 'user/history.html', {'table': table})


def user_specific_file_history(request, pk):
    user = request.user
    file = DocumentFile.objects.get(pk=pk)
    if user and file:

        if user.is_superuser:
            table = SpecificFileUserHistoryTable(Modification.objects.filter(file=file))
            return render(request, 'user/specific_user_file_history.html', {'table': table,
                                                                            'file': file.__str__()})
        else:
            table = SpecificFileUserHistoryTable(Modification.objects.filter(by=user, file=file))
            return render(request, 'user/specific_user_file_history.html', {'table': table,
                                                                            'file': file.__str__()})


def file_details(request, pk):
    file = DocumentFile.objects.get(pk=pk)
    data = {}
    if file:
        data.update({'file': file})
        file_stage = file.stage
        perms_codename = ""
        if file_stage == STAGES[0]:
            perms_codename = "can_register_batch"
        elif file_stage == STAGES[1]:
            perms_codename = "can_receive_file"
        elif file_stage == STAGES[2]:
            perms_codename = "can_disassemble_file"
        elif file_stage == STAGES[3]:
            perms_codename = "can_scan_file"
        elif file_stage == STAGES[4]:
            perms_codename = "can_transcribe_file"
        elif file_stage == STAGES[5]:
            perms_codename = "can_qa_file"
        elif file_stage == STAGES[6]:
            perms_codename = "can_validate_file"

        stage_index = STAGES.index(file.stage)
        last_update = file.modification_set.last()
        last_update_data = file.created_on
        if last_update:
            last_update_data = last_update.created_at
        users = User.objects.filter(groups__permissions__codename=perms_codename)
        data.update({'users': users, 'last_update': last_update_data})

    return render(request, 'file/details.html', data)


def assign_file(request, pk):
    if request.method == 'POST':
        user = User.objects.get(pk=request.POST.get('assigned_user'))

        file = DocumentFile.objects.get(pk=pk)
        if user:
            if file:
                file.assigned_to = user
                file.flagged = False
                file.save()
                messages.success(request, 'Assigned successfully ')

    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
