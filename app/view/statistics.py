from django.shortcuts import render
from .logged_in_users import get_current_users


def statistics(request):
    logged_in_users = get_current_users()
    context = {'logged_in':logged_in_users}
    return render(request, 'global/statistics.html', context)
