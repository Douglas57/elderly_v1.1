# add notes to file or document or batch
import json

from django.shortcuts import get_object_or_404
from django.views.decorators.csrf import csrf_exempt

from app.models import DocumentFile, DocumentFileDetail, Batch

@csrf_exempt
def update_misc(request):
    """Do json updates on the document"""
    type = ''
    pk = ''
    comment = ''
    if type == 'file':
        file = get_object_or_404(DocumentFile, pk)
        file.miscellaneous = comment
        file.save()
    elif type == 'document':
        document = get_object_or_404(DocumentFile, pk)
        document.miscellaneous = comment
        document.save()
    elif type == 'batch':
        batch = get_object_or_404(DocumentFile, pk)
        batch.miscellaneous = comment
        batch.save()

