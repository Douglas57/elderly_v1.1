import os
import time
import queue
from background_task import background
from app.models import DocumentFile, Filer
from django.contrib.auth.models import User
from PyPDF2 import PdfFileReader, PdfFileWriter


@background(schedule=60)
def split_files(file_pk):
    q = queue.Queue
    files = Filer.objects.filter(file_reference_id=file_pk).values_list('filepond', flat=True)

    completed = []
    for file in files:
        q.put(file)

    while not q.empty():
        doc_pdf = q.get()

        directory = os.path.dirname(doc_pdf)

        try:
            inputpdf = PdfFileReader(open("media/" + doc_pdf, "rb"))

            completed.append(doc_pdf)

            for i in range(inputpdf.getNumPages()):
                output = PdfFileWriter()
                output.addPage(inputpdf.getPage(i))

                with open("media/" + directory + "/%s.pdf" % i, "wb") as outputStream:
                    output.write(outputStream)  #
                print('================>{}==========>{}.pdf'.format(doc_pdf, i))
        except Exception as exc:
            print(exc)
            continue


@background(schedule=60)
def clean_up_split_files(file_reference):
    file = Filer.objects.filter(file_reference_id=file_reference).values_list('filepond', flat=True)
    directory = "media/" + os.path.dirname(file[0])
    for file in os.listdir(directory):
        basename = os.path.basename(file)
        file_name = os.path.splitext(basename)[0]
        print(file_name)
        if file_name.isdigit():
            os.remove(directory + file)

    split_files(file_reference)


@background(schedule=60)
def test_tasks():
    print("running the tasks")
    f = open('sql.txt', 'a+')
    f.write("we keep running after time")
    f.close()
