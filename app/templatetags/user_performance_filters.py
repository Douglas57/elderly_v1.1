from datetime import date,datetime, timedelta

from django import template
from django.contrib.auth.models import User
import urllib

register = template.Library()
from app.models import Profile


@register.filter
def get_guardians(user=None):

        profiles=Profile.objects.filter(elderly_id=user)
        if not profiles == None:
                user_ids=[]
                for profile in profiles:
                        user_ids.append(profile.user_id)

                user=User.objects.filter(pk__in=user_ids)
        else:
                user=None

        return user




