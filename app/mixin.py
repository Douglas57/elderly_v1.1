from django.shortcuts import redirect
from  app.manual_splitting import split_pdfs
class LoggedInRedirectMixin:

    def dispatch(self, request, *args, **kwargs):


        if request.user.is_authenticated:
            return redirect('profile')


        else:
            return super().dispatch(request, *args, **kwargs)
