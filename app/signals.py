

from django.dispatch import Signal
from django.db.models.signals import post_save
from django.contrib.auth.models import User
from django.dispatch import receiver
from .models import Profile
from app.auto_splitting import split_pdfs


file_uploaded = Signal(providing_args=["file_pk"])
filemanager_pre_upload = Signal(providing_args=["filename", "path", "filepath"])
filemanager_post_upload = Signal(providing_args=["filename", "path", "filepath"])
@receiver(post_save, sender=User)
def create_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_profile(sender, instance, **kwargs):
    instance.profile.save()

@receiver(file_uploaded)
def split(sender,**kwargs):
    split_pdfs(file_pk=kwargs['file_pk'])
    return

