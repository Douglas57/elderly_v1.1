import itertools

import django_tables2 as tables
from django.utils.html import format_html
from django.utils.safestring import mark_safe
from django_tables2 import TemplateColumn, A
from .models import DocumentFile, DocumentFileDetail, Batch, Modification, Stock, STAGES, STATES,Money_disburdment
from django.urls import reverse
from django.contrib.auth.models import User, Group


def color_codes(record):
    if record.stage == STAGES[0]:
        return "background-color:rgba(75, 0, 130,0.3)"
    elif record.stage == STAGES[1]:
        return "background-color:rgba(255, 0, 128,0.3)"
    elif record.stage == STAGES[2]:
        return "background-color:rgba(255,69,0,0.3)"
    elif record.stage == STAGES[3]:
        return "background-color:rgba(128, 128, 0,0.3)"
    elif record.stage == STAGES[4]:
        return "background-color:rgba(178,34,34,0.3)"
    elif record.stage == STAGES[5]:
        return "background-color:rgba(0, 0, 128,0.3)"
    elif record.stage == STAGES[6]:
        return "background-color:rgba(255,182,193,0.3)"
    elif record.stage == STAGES[7]:
        return "background-color:rgba(153,50,204,0.3)"
    else:
        return "background-color:rgba(0,255,239,0.3)"


class FileCheckboxColumn(tables.CheckBoxColumn):
    def render(self, value, bound_column, record):
        default = {"type": "checkbox", "name": bound_column.name, "value": value}
        if self.is_checked(value, record):
            default.update({"checked": "checked"})
        general = self.attrs.get("input")
        specific = self.attrs.get("td_input")
        attrs = tables.utils.AttributeDict(default, **(specific or general or {}))
        return mark_safe("<p><label><input %s/><span><span></label></p>" % attrs.as_html())


class BatchTable(tables.Table):
    counter = tables.Column(empty_values=(), orderable=False)

    class Meta:
        attrs = {"class": "table table-bordered table-striped table-responsive"}
        model = Batch
        template_name = "django_tables2/bootstrap4.html"
        fields = ("counter", "batch_no", "created_on", "created_by", "state", "description")

    files = TemplateColumn(template_name='batch/total_column.html', orderable=False)
    actions = TemplateColumn(template_name='batch/view_column.html', orderable=False)

    def render_counter(self):
        self.row_counter = getattr(self, 'row_counter', itertools.count(start=1))
        return next(self.row_counter)


class AdminBatchTable(tables.Table):
    counter = tables.Column(empty_values=(), orderable=False)

    class Meta:
        attrs = {"class": "table table-bordered table-striped table-responsive"}
        model = Batch
        template_name = "django_tables2/bootstrap4.html"
        fields = ("counter", "batch_no", "created_on", "created_by", "state", 'batch_type', 'group', "description")

    files = TemplateColumn(template_name='batch/total_column.html', orderable=False)
    actions = TemplateColumn(template_name='batch/view_column.html', orderable=False)

    def render_counter(self):
        self.row_counter = getattr(self, 'row_counter', itertools.count(start=1))
        return next(self.row_counter)


class CompleteBatchTable(tables.Table):
    counter = tables.Column(empty_values=(), orderable=False)

    class Meta:
        attrs = {"class": "table table-bordered table-striped table-responsive"}
        model = Batch
        template_name = "django_tables2/bootstrap4.html"
        fields = ("counter", "batch_no", "created_at", "created_by", "acceptance_status")

    files = TemplateColumn(template_name='batch/total_column.html', orderable=False)
    actions = TemplateColumn(template_name='batch/accept_column.html', orderable=False)

    def render_counter(self):
        self.row_counter = getattr(self, 'row_counter', itertools.count(start=1))
        return next(self.row_counter)


class StockTable(tables.Table):
    counter = tables.Column(empty_values=(), orderable=False)

    class Meta:
        attrs = {"class": "table table-bordered table-striped table-responsive"}
        model = Stock
        template_name = "django_tables2/bootstrap4.html"
        fields = (
            'counter', 'file_number', 'name', 'nationality', 'cross_reference', 'file_category',
            'date_last_correspondence',
            'date_first_correspondence', 'location_of_file', 'comment')

    actions = TemplateColumn(template_name='stock/view_column.html', orderable=False)

    def render_counter(self):
        self.row_counter = getattr(self, 'row_counter', itertools.count(start=1))
        return next(self.row_counter)

    def render_date_first_correspondence(self, value, record):
        return mark_safe(f"{record.date_first_correspondence.strftime('%d/%m/%Y')}")

    def render_date_last_correspondence(self, value, record):
        return mark_safe(f"{record.date_last_correspondence.strftime('%d/%m/%Y')}")


class ReturnBatchTable(tables.Table):
    # transitions = tables.Column(accessor='get_transition_options',verbose_name='Transition')
    counter = tables.Column(empty_values=(), orderable=False)

    class Meta:
        attrs = {"class": "table table-bordered table-striped table-responsive"}
        model = Batch
        template_name = "django_tables2/bootstrap4.html"
        fields = ("counter", "batch_no", "created_on", "created_by", "state", "description")

    files = TemplateColumn(template_name='batch/total_column.html', orderable=False)
    actions = TemplateColumn(template_name='batch/view_column.html', orderable=False)

    def render_counter(self):
        self.row_counter = getattr(self, 'row_counter', itertools.count(start=1))
        return next(self.row_counter)


class BatchFileTable(tables.Table):
    counter = tables.Column(empty_values=(), orderable=False)

    class Meta:
        attrs = {"class": "table table-bordered table-striped"}
        row_attrs = {
            "style": lambda record: color_codes(record)
        }
        model = DocumentFile
        template_name = "django_tables2/bootstrap4.html"
        fields = (
            "counter", "original_batch", "file_reference", "file_type", "state", "stage", "file_created_by",
            "file_barcode", "created_on")

    file_reference = TemplateColumn(template_name='file/file_reference_column.html', orderable=True)
    move_stage = TemplateColumn(template_name='batch/file_view_column.html', orderable=False)
    docs = TemplateColumn(template_name='file/total_column.html', orderable=False)
    change_state = TemplateColumn(template_name='batch/file_state_column.html', orderable=False)

    def render_counter(self):
        self.row_counter = getattr(self, 'row_counter', itertools.count(start=1))
        return next(self.row_counter)


class CompleteBatchFileTable(tables.Table):
    counter = tables.Column(empty_values=(), orderable=False)

    class Meta:
        attrs = {"class": "table table-bordered table-striped"}
        row_attrs = {
            "style": lambda record: color_codes(record)
        }
        model = DocumentFile
        template_name = "django_tables2/bootstrap4.html"
        fields = (
            "counter", "file_reference", "file_type", "state", "stage", "file_created_by", "file_barcode", "created_on")

    def render_file_reference(self, value, record):
        url = reverse('file_details', kwargs={'pk': record.pk})
        return mark_safe(f'<a href="{url}"><strong>{value}</strong></a>')

    docs = TemplateColumn(template_name='file/total_column.html', orderable=False)
    actions = TemplateColumn(template_name='batch/complete_action.html', orderable=False)

    def render_counter(self):
        self.row_counter = getattr(self, 'row_counter', itertools.count(start=1))
        return next(self.row_counter)


class BatchDocumentTable(tables.Table):
    counter = tables.Column(empty_values=(), orderable=False)

    class Meta:
        attrs = {"class": "table table-bordered table-striped"}
        model = DocumentFileDetail
        template_name = "django_tables2/bootstrap4.html"
        fields = ("counter", "file_reference_id", "document_barcode", "state", "document_name_id", "document_file_path")

    actions = TemplateColumn(template_name='app/document_transcribe.html', orderable=False)

    def render_counter(self):
        self.row_counter = getattr(self, 'row_counter', itertools.count(start=1))
        return next(self.row_counter)


class DocumentFileTable(tables.Table):
    counter = tables.Column(empty_values=(), orderable=False)
    check = tables.CheckBoxColumn(accessor='pk', attrs={"th__input": {"onclick": "toggle(this)"}}, orderable=False)
    user = TemplateColumn(template_name='file/assigned_to.html', orderable=False)

    class Meta:
        attrs = {"class": "table table-bordered table-striped"}
        row_attrs = {
            "style": lambda record: color_codes(record)
        }
        model = DocumentFile

        template_name = "django_tables2/bootstrap4.html"
        fields = ("check", "counter", "file_reference", "file_type", "state", "stage", "file_barcode", "created_on")

    action = TemplateColumn(template_name='file/view_column.html')
    docs = TemplateColumn(template_name='file/total_column.html', orderable=False)
    change_state = TemplateColumn(template_name='batch/file_state_column.html')

    file_reference = TemplateColumn(template_name='file/file_reference_column.html', orderable=True)

    def render_counter(self):
        self.row_counter = getattr(self, 'row_counter', itertools.count(start=1))
        return next(self.row_counter)


class DocumentFileQATable(tables.Table):
    counter = tables.Column(empty_values=(), orderable=False)
    check = tables.CheckBoxColumn(accessor='pk', attrs={"th__input": {"onclick": "toggle(this)"}}, orderable=False)
    user = TemplateColumn(template_name='file/assigned_to.html', orderable=False)

    class Meta:
        attrs = {"class": "table table-bordered table-striped"}
        row_attrs = {
            "style": lambda record: color_codes(record)
        }
        model = DocumentFile

        template_name = "django_tables2/bootstrap4.html"
        fields = ("check", "counter", "file_reference", "file_type", "stage", "file_barcode", "created_on")

    # action = TemplateColumn(template_name='file/view_column.html')
    docs = TemplateColumn(template_name='file/total_column.html', orderable=False)
    # change_state = TemplateColumn(template_name='batch/file_state_column.html')

    file_reference = TemplateColumn(template_name='file/file_reference_column.html', orderable=True)

    def render_counter(self):
        self.row_counter = getattr(self, 'row_counter', itertools.count(start=1))
        return next(self.row_counter)


class EscalatedFileTable(tables.Table):
    counter = tables.Column(empty_values=(), orderable=False)
    comment = tables.Column(empty_values=(), orderable=False)

    class Meta:
        attrs = {"class": "table table-bordered table-striped"}
        row_attrs = {
            "class": lambda record: "bg-warning" if record.flagged else "bg-default"
        }
        model = DocumentFile
        template_name = "django_tables2/bootstrap4.html"
        fields = ("counter", "file_reference", "file_type", "state","stage","file_barcode","comment")

    file_reference = TemplateColumn(template_name='file/file_reference_column.html', orderable=True)


    action = TemplateColumn(template_name='file/view_column.html')
    docs = TemplateColumn(template_name='file/total_column.html', orderable=False)
    change_state = TemplateColumn(template_name='batch/file_state_column.html')

    # transcribe = TemplateColumn(template_name='app/document_action_column.html')
    def render_comment(self,record):
        from django.utils.html import strip_tags
        #self.row_counter = getattr(self, 'row_counter', itertools.count(start=1))
        return strip_tags(record.notification_set.last().comment)

    def render_counter(self):
        self.row_counter = getattr(self, 'row_counter', itertools.count(start=1))
        return next(self.row_counter)


class DocumentTable(tables.Table):
    counter = tables.Column(empty_values=(), orderable=False)

    class Meta:
        attrs = {"class": "table table-bordered table-striped"}
        model = DocumentFileDetail
        template_name = "django_tables2/bootstrap4.html"
        fields = ("counter", "file_reference", "document_barcode", "document_file_path", "document_type")

    status = TemplateColumn(template_name='app/document_status.html', orderable=False)
    actions = TemplateColumn(template_name='app/document_transcribe.html', orderable=False)

    def render_counter(self):
        self.row_counter = getattr(self, 'row_counter', itertools.count(start=1))
        return next(self.row_counter)


class ValidationTable(tables.Table):
    counter = tables.Column(empty_values=(), orderable=False)

    class Meta:
        attrs = {"class": "table table-bordered table-striped"}
        row_attrs = {
            "style": lambda record: color_codes(record)
        }
        model = DocumentFile
        template_name = "django_tables2/bootstrap4.html"
        fields = ("counter", "file_reference", "file_type", "state", "file_created_by", "file_barcode", "created_on")

    file_reference = TemplateColumn(template_name='file/file_reference_column.html', orderable=True)

    docs = TemplateColumn(template_name='file/total_column.html', orderable=False)
    validate = TemplateColumn(template_name='file/validator_column.html', orderable=False)

    def render_counter(self):
        self.row_counter = getattr(self, 'row_counter', itertools.count(start=1))
        return next(self.row_counter)


class QaTable(tables.Table):
    counter = tables.Column(empty_values=(), orderable=False)

    class Meta:
        attrs = {"class": "table table-bordered table-striped"}
        row_attrs = {
            "style": lambda record: color_codes(record)
        }
        model = DocumentFile
        template_name = "django_tables2/bootstrap4.html"
        fields = ("counter", "file_reference", "file_type", "state", "file_created_by", "file_barcode", "created_on")

    file_reference = TemplateColumn(template_name='file/file_reference_column.html', orderable=True)

    docs = TemplateColumn(template_name='file/total_column.html', orderable=False)

    action = TemplateColumn(template_name='file/qa_column.html', orderable=False)

    def render_counter(self):
        self.row_counter = getattr(self, 'row_counter', itertools.count(start=1))
        return next(self.row_counter)


class ScannerTable(tables.Table):
    counter = tables.Column(empty_values=(), orderable=False)

    class Meta:
        attrs = {"class": "table table-bordered table-striped"}
        row_attrs = {
            "style": lambda record: color_codes(record)
        }
        model = DocumentFile
        template_name = "django_tables2/bootstrap4.html"
        fields = ("counter", "file_reference", "state", "file_barcode", "created_on")

    file_reference = TemplateColumn(template_name='file/file_reference_column.html', orderable=True)

    docs = TemplateColumn(template_name='file/total_column.html', orderable=False)
    action = TemplateColumn(template_name='file/scan.html', orderable=False)

    def render_counter(self):
        self.row_counter = getattr(self, 'row_counter', itertools.count(start=1))
        return next(self.row_counter)


class TranscribeTable(tables.Table):
    counter = tables.Column(empty_values=(), orderable=False)

    class Meta:
        attrs = {"class": "table table-bordered table-striped"}
        row_attrs = {
            "style": lambda record: color_codes(record)
        }
        model = DocumentFile
        template_name = "django_tables2/bootstrap4.html"
        fields = ("counter", "file_reference", "file_type", "state", "file_created_by", "file_barcode", "created_on")

    file_reference = TemplateColumn(template_name='file/file_reference_column.html', orderable=True)

    # transcribe = TemplateColumn(template_name='app/document_action_column.html', orderable=False)
    action = TemplateColumn(template_name='file/view_column.html', orderable=False)

    def render_counter(self):
        self.row_counter = getattr(self, 'row_counter', itertools.count(start=1))
        return next(self.row_counter)


class HistoryTable(tables.Table):
    counter = tables.Column(empty_values=(), orderable=False)

    class Meta:
        attrs = {"class": "table table-bordered table-striped"}
        model = Modification
        template_name = "django_tables2/bootstrap4.html"
        fields = ("counter", "file", "modified_from_stage", "modified_to_stage", "by", "created_at")

    view = TemplateColumn(template_name='file/view_history.html')

    def render_counter(self):
        self.row_counter = getattr(self, 'row_counter', itertools.count(start=1))
        return next(self.row_counter)


class SpecificFileUserHistoryTable(tables.Table):
    counter = tables.Column(empty_values=(), orderable=False)

    class Meta:
        attrs = {"class": "table table-bordered table-striped"}
        model = Modification
        template_name = "django_tables2/bootstrap4.html"
        fields = ("counter", "modified_from_stage", "modified_to_stage", "created_at", "by")

    def render_counter(self):
        self.row_counter = getattr(self, 'row_counter', itertools.count(start=1))
        return next(self.row_counter)


class AdminTable(tables.Table):
    counter = tables.Column(empty_values=(), orderable=False)

    class Meta:
        attrs = {"class": "table table-bordered table-striped"}
        row_attrs = {
            "style": lambda record: color_codes(record)
        }
        model = DocumentFile
        template_name = "django_tables2/bootstrap4.html"
        fields = ("counter", "file", 'file_reference')

    file_reference = TemplateColumn(template_name='file/file_reference_column.html', orderable=True)

    def render_counter(self):
        self.row_counter = getattr(self, 'row_counter', itertools.count(start=1))
        return next(self.row_counter)


class ValidateQADocTable(tables.Table):
    counter = tables.Column(empty_values=(), orderable=False)

    class Meta:
        attrs = {"class": "table table-bordered table-striped"}
        model = DocumentFileDetail
        template_name = "django_tables2/bootstrap4.html"
        fields = ("counter", "file_reference_id", "document_barcode", "document_file_path","document_type")

    actions = TemplateColumn(template_name='document/inspect.html', orderable=False)

    file_reference = TemplateColumn(template_name='file/file_reference_column.html', orderable=True)

    def render_counter(self):
        self.row_counter = getattr(self, 'row_counter', itertools.count(start=1))
        return next(self.row_counter)


class ReceiverTable(tables.Table):
    counter = tables.Column(empty_values=(), orderable=False)

    class Meta:
        attrs = {"class": "table table-bordered table-striped table-responsive"}
        model = Batch
        template_name = "django_tables2/bootstrap4.html"
        fields = ("counter", "batch_no", "created_on", "state", "description")

    files = TemplateColumn(template_name='batch/total_column.html', orderable=False)
    actions = TemplateColumn(template_name='batch/receive_column.html', orderable=False)

    def render_counter(self):
        self.row_counter = getattr(self, 'row_counter', itertools.count(start=1))
        return next(self.row_counter)


class ReceiverFiles(tables.Table):
    counter = tables.Column(empty_values=(), orderable=False)

    class Meta:
        attrs = {"class": "table table-bordered table-striped"}
        row_attrs = {
            "style": lambda record: color_codes(record)
        }
        model = DocumentFile
        template_name = "django_tables2/bootstrap4.html"
        fields = ("counter", "file_reference", "file_type", "stage", "file_created_by", "file_barcode", "created_on")

    file_reference = TemplateColumn(template_name='file/file_reference_column.html', orderable=True)

    docs = TemplateColumn(template_name='file/total_column.html', orderable=False)
    move_stage = TemplateColumn(template_name='batch/receiver_actions.html', orderable=False)

    def render_counter(self):
        self.row_counter = getattr(self, 'row_counter', itertools.count(start=1))
        return next(self.row_counter)


class AssemblerFiles(tables.Table):
    counter = tables.Column(empty_values=(), orderable=False)

    class Meta:
        attrs = {"class": "table table-bordered table-striped"}
        row_attrs = {
            "style": lambda record: color_codes(record)
        }
        model = DocumentFile

        template_name = "django_tables2/bootstrap4.html"
        fields = (
            "counter", "file_reference", "file_type", "state", "stage", "file_created_by", "file_barcode", "created_on")

    file_reference = TemplateColumn(template_name='file/file_reference_column.html', orderable=True)

    docs = TemplateColumn(template_name='file/total_column.html', orderable=False)
    move_stage = TemplateColumn(template_name='batch/assembly_column.html', orderable=False)

    def render_counter(self):
        self.row_counter = getattr(self, 'row_counter', itertools.count(start=1))
        return next(self.row_counter)


class AssemblerDocuments(tables.Table):
    counter = tables.Column(empty_values=(), orderable=False)

    class Meta:
        attrs = {"class": "table table-bordered table-striped"}
        model = DocumentFileDetail
        template_name = "django_tables2/bootstrap4.html"
        fields = ("counter", "file_reference_id", "file_reference", "document_barcode", "state","document_type")

    actions = TemplateColumn(template_name='app/assembler_document_actions.html', orderable=False)

    file_reference = TemplateColumn(template_name='file/file_reference_column.html', orderable=True)

    def render_counter(self):
        self.row_counter = getattr(self, 'row_counter', itertools.count(start=1))
        return next(self.row_counter)


class CompleteFiles(tables.Table):
    counter = tables.Column(empty_values=(), orderable=False)
    validated = tables.Column(empty_values=(), orderable=False)

    class Meta:
        attrs = {"class": "table table-bordered table-striped"}
        row_attrs = {
            "style": lambda record: color_codes(record)
        }
        model = DocumentFile
        template_name = "django_tables2/bootstrap4.html"
        fields = ("counter", "file_reference", "file_type", "state", "stage", "file_barcode", "created_on", "validated")

    file_reference = TemplateColumn(template_name='file/file_reference_column.html', orderable=True)

    docs = TemplateColumn(template_name='file/total_column.html', orderable=False)
    open = TemplateColumn(template_name='file/open_eye.html', orderable=False)

    def render_validated(self, record):
        html_class = 'badge-danger '
        if record.validated:
            html_class = 'badge-success'

        return mark_safe(f'<span class=" badge {html_class}">{record.validated}</span>')

    def render_counter(self):
        self.row_counter = getattr(self, 'row_counter', itertools.count(start=1))
        return next(self.row_counter)


class UserTable(tables.Table):
    counter = tables.Column(empty_values=(), orderable=False)
    ID_Number = tables.Column(empty_values=())
    Fullname = tables.Column(empty_values=())
    password_set = tables.Column(empty_values=())

    class Meta:
        attrs = {"class": "table table-bordered table-striped table-responsive"}
        model = User
        template_name = "django_tables2/bootstrap4.html"
        fields = (
            'counter', 'Fullname', 'username', 'email', 'ID_Number', 'groups', 'is_active', 'is_superuser',
            'password_set')

    actions = TemplateColumn(template_name='user/action_column.html', orderable=False)

    def render_counter(self):
        self.row_counter = getattr(self, 'row_counter', itertools.count(start=1))
        return next(self.row_counter)

    def render_ID_Number(self, record):
        return mark_safe(f'{record.profile.id_no}')

    def render_Fullname(self, record):
        return mark_safe(f'{record.profile.full_name}')

    def render_password_set(self, record):
        html_class = 'badge-danger'
        if not record.profile.first_login:
            html_class = 'badge-success'

        return mark_safe(f'<span class=" badge {html_class}">{not record.profile.first_login}</span>')


class GroupTable(tables.Table):
    counter = tables.Column(empty_values=(), orderable=False)

    class Meta:
        attrs = {"class": "table table-bordered table-striped table-responsive"}
        model = Group
        template_name = "django_tables2/bootstrap4.html"
        fields = ('counter', 'name', 'permissions')

    actions = TemplateColumn(template_name='group/action_column.html', orderable=False)

    def render_counter(self):
        self.row_counter = getattr(self, 'row_counter', itertools.count(start=1))
        return next(self.row_counter)

class DispatchTable(tables.Table):
    counter = tables.Column(empty_values=(), orderable=False)

    class Meta:
        attrs = {"class": "table table-bordered table-striped table-responsive"}
        model = Money_disburdment
        template_name = "django_tables2/bootstrap4.html"
        fields = ('counter', 'user', 'created_at')

    def render_counter(self):
        self.row_counter = getattr(self, 'row_counter', itertools.count(start=1))
        return next(self.row_counter)

class CleanUpTable(tables.Table):
    counter = tables.Column(empty_values=(), orderable=False)

    class Meta:
        attrs = {"class": "table table-bordered table-striped"}
        model = DocumentFile
        template_name = "django_tables2/bootstrap4.html"
        fields = ("counter", "file_reference", "stage", "file_barcode", "created_on")

    # file_reference = TemplateColumn(template_name='file/file_reference_column.html', orderable=True)

    docs = TemplateColumn(template_name='file/total_column.html', orderable=False)
    danger = TemplateColumn(template_name='file/split.html', orderable=False)

    def render_validated(self, record):
        html_class = 'badge-danger '
        if record.validated:
            html_class = 'badge-success'

        return mark_safe(f'<span class=" badge {html_class}">{record.validated}</span>')

    def render_counter(self):
        self.row_counter = getattr(self, 'row_counter', itertools.count(start=1))
        return next(self.row_counter)