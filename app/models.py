import os
from datetime import datetime
from enum import Enum
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.contrib.postgres.fields import JSONField
from django.utils import timezone
from django_fsm import FSMField, transition, has_transition_perm
from django.contrib.auth.models import User, Permission, Group
from PIL import Image
from django.urls import reverse
from django.contrib.postgres.indexes import BrinIndex

import app

from .utils import is_assigned
from .validators import start_with_A_validator, start_with_B_validator, start_with_C_validator

STAGES = ("Registry", "Reception", "Assembly", "Scanner", "Transcriber", "Quality Assurance", "Validator", "Complete")
STATES = ("Opened", "Closed")
BATCH = ("Opened", "Done", "Closed")

PREPARE_DIGITIZATION = 'PREPARE_DIGITIZATION'
PREPARE_DIGITIZATION_INTERMEDIARY = 'PREPARE_DIGITIZATION_INTERMEDIARY'
RETURN_ON_DIGITIZATION = 'RETURN_ON_DIGITIZATION'
DIGITIZED_AND_ACCEPTED = 'DIGITIZED_AND_ACCEPTED'
DIGITIZED_AND_REJECTED = 'DIGITIZED_AND_REJECTED'
FLAGGED = 'FLAGGED'
FLAGGED_AND_CORRECTED = 'FLAGGED_AND_CORRECTED'
FLAGGED_AND_CORRECTED_INTERMEDIARY = 'FLAGGED_AND_CORRECTED_INTERMEDIARY'
AWAITING_DIGITIZATION = 'AWAITING_DIGITIZATION'
PREPARE_FLAGGING = 'PREPARE_FLAGGING'
PREPARE_FLAGGING_INTERMEDIARY = 'PREPARE_FLAGGING_INTERMEDIARY'
ERRORS_FIXED = 'ERRORS_FIXED'
SCANNED = 'SCANNED'
DIGITIZED = 'DIGITIZED'
DIGITIZED_INTERMEDIARY = 'DIGITIZED_INTERMEDIARY'
BATCH_TYPES = [
    ('PREPARE_DIGITIZATION', 'Prepare for Digitization'),
    ('PREPARE_DIGITIZATION_INTERMEDIARY', 'Awaiting Approval for Digitization'),
    ('RETURN_ON_DIGITIZATION', 'Returned on Digitization'),
    ('DIGITIZED_AND_ACCEPTED', 'Digitized and accepted'),
    ('DIGITIZED_AND_REJECTED', 'Digitized and rejected'),
    ('FLAGGED', 'Flagged'),
    ('FLAGGED_AND_CORRECTED', 'Flagged and corrected'),
    ('FLAGGED_AND_CORRECTED_INTERMEDIARY', 'Awaiting approval after error fixed'),
    ('AWAITING_DIGITIZATION', 'Awaiting Digitization'),
    ('PREPARE_FLAGGING', 'Prepare for flagging'),
    ('PREPARE_FLAGGING_INTERMEDIARY', 'Awaiting approval after Flagging'),
    ('ERRORS_FIXED', 'Errors fixed'),
    ('SCANNED', 'Scanned'),
    ('DIGITIZED', 'Digitized'),
    ('DIGITIZED_INTERMEDIARY', 'Awaiting approval'),
]


class Batch(models.Model):
    class Meta:
        permissions = [('can_register_batch', 'Can Register Batch ')]
        indexes = [(BrinIndex(fields=['batch_no'], name='batch_batch_no_idx', db_tablespace='pg_default'))

                   ]

    batch_no = models.CharField(max_length=255, null=False, unique=True, validators=[start_with_B_validator])
    batch_type = models.CharField(max_length=255, default=PREPARE_DIGITIZATION, choices=BATCH_TYPES)
    group = models.ForeignKey(Group, on_delete=models.SET_NULL, null=True)
    created_on = models.DateTimeField(auto_now_add=timezone.now)
    created_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, null=True, blank=True,
                                   related_name='created_by', db_index=True, db_tablespace='pg_default')
    is_return_batch = models.BooleanField(default=False)
    state = FSMField(default=BATCH[0], protected=True)
    description = models.TextField(null=True, blank=True)
    miscellaneous = JSONField(null=True)

    def get_absolute_url(self):
        return reverse('batch_files', kwargs={'pk': self.pk})

    # transition methods
    @transition(field=state, source=[BATCH[2]], target=BATCH[0])
    def open(self, user=None):

        """opening a closed file"""
        if self.batch_type == PREPARE_DIGITIZATION:
            files = DocumentFile.objects.filter(batch=self)
            for file in files:
                file.open(user)
                file.save()

    @transition(field=state, source=[BATCH[0]], target=BATCH[1])
    def done(self, user):

        self.save()

    @transition(field=state, source=[BATCH[1]], target=BATCH[0])
    def continue_editing(self):

        self.save()

    @transition(field=state, source=[BATCH[1]], target=BATCH[2])
    def close(self, user=None, comment=''):
        """"closes a batch
                 -moves state of files in batch to CLOSE
                -moves batch state to CLOSE
                -moves stage files in batch to RECEPTION

                this action is permanent
                 """

        try:
            if self.batch_type == PREPARE_DIGITIZATION:
                self.move_prepare_digitization_intermediary(user)
            elif self.batch_type == PREPARE_FLAGGING and self.group == self.get_reception_group():
                self.return_batch_files_to_registry(user, onErros=True)
                self.group = self.get_registry_group()
                self.save()

        except Exception:
            raise Exception

    @staticmethod
    def get_registry_group():
        return Group.objects.filter(permissions__codename='can_register_batch').first()

    @staticmethod
    def get_reception_group():
        return Group.objects.filter(permissions__codename='can_receive_file').first()

    def move_prepare_digitization_intermediary(self, user):

        self.move_batch_files_to_reception(user)
        self.group = Batch.get_reception_group()
        self.save()

    def receive_prepare_flagging(self, user, approved=True):
        if approved:
            self.return_batch_files_to_registry(user, onErros=True)
            self.batch_type = FLAGGED
            self.group = Batch.get_registry_group()
        else:
            self.group = Batch.get_reception_group()
            self.batch_type = PREPARE_FLAGGING_INTERMEDIARY

        self.save()

    def receive_ready_for_digitization(self, user, approved=False):
        if approved:
            self.batch_type = AWAITING_DIGITIZATION
            self.group = Batch.get_reception_group()
        else:

            self.return_batch_files_to_registry(user, onErros=False)
            self.batch_type = PREPARE_DIGITIZATION_INTERMEDIARY

            self.group = Batch.get_registry_group()
        self.save()

    def move_batch_files_to_reception(self, user):
        files = DocumentFile.objects.filter(batch=self)
        for file in files:
            if file.state == STATES[0] and file.stage == STAGES[0]:
                file.close(user=user)
                file.dispatch_reception(user=user)
                file.save()

    def return_batch_files_to_registry(self, user, onErros=False):
        files = DocumentFile.objects.filter(batch=self)
        if onErros:
            for file in files:
                file.notify_on_return_registry()
                file.flagged = True
                file.assigned_to = file.file_created_by
                file.save()
        else:
            for file in files:
                # file.close(user=user)
                # file.return_registry(user=user,rejection_comment='')
                # file.assigned_to = user
                file.flagged = False
                file.save()

    def get_transition_options(self):
        transition = list(self.get_available_state_transitions())
        # "Opened", "Done", "Closed"
        # 'Open', 'Done', 'Continue_Editing', 'Close'
        if transition[0].target == "Opened":
            return "Open"
        elif transition[0].target == "Done":
            return "Done"
        else:
            return "Close"

    def __str__(self):
        return self.batch_no


class DocumentFileType(models.Model):
    file_type = models.CharField(max_length=100, null=False, primary_key=True)
    file_description = models.CharField(max_length=255)

    def __str__(self):
        return self.file_type


class DocumentType(models.Model):
    document_name = models.CharField(max_length=255, primary_key=True)
    document_field_specs = JSONField()
    document_description = models.CharField(max_length=255)

    def __str__(self):
        return self.document_name


#
class DocumentFile(models.Model):
    class Meta:
        permissions = [('can_receive_file', 'Can Receive File'),
                       ('can_disassemble_file', 'Can Disassemble or Assemble File'),
                       ('can_scan_file', 'Can Scan File'),
                       ('can_transcribe_file', 'Can Transcribe File'),
                       ('can_qa_file', 'Can Qa File'),
                       ('can_validate_file', 'Can Validate')
                       ]
        indexes = [
            (BrinIndex(fields=['file_reference'], name='file_file_reference_idx', db_tablespace='pg_default')),
            (BrinIndex(fields=['file_barcode'], name='file_file_barcode_idx', db_tablespace='pg_default')),
            (BrinIndex(fields=['stage'], name='file_stage_idx', db_tablespace='pg_default')),

        ]

    file_reference = models.CharField(unique=True, max_length=100)
    file_type = models.ForeignKey(DocumentFileType, on_delete=models.CASCADE)

    batch = models.ForeignKey(Batch, on_delete=models.DO_NOTHING)
    original_batch = models.ForeignKey(Batch, on_delete=models.DO_NOTHING, null=True, related_name='original_batch')
    file_created_by = models.ForeignKey(User,
                                        on_delete=models.DO_NOTHING,
                                        related_name='file_created_by', db_index=True, db_tablespace='pg_default')

    created_on = models.DateTimeField(auto_now_add=timezone.now)
    state = FSMField(default=STATES[0], protected=True)

    file_barcode = models.CharField(unique=True, max_length=255, validators=[start_with_A_validator])
    flagged = models.BooleanField(default=False)

    assigned_to = models.ForeignKey(User, null=True, blank=True,
                                    on_delete=models.DO_NOTHING,
                                    related_name='file_assigned_to', db_index=True, db_tablespace='pg_default')
    lock = models.BooleanField(default=False)
    file_path = models.CharField(null=True, max_length=100)
    stage = FSMField(default=STAGES[0], protected=True)
    miscellaneous = JSONField(null=True)

    def save(self, *args, **kwargs):
        if not self.pk:
            self.original_batch = self.batch
        super().save(*args, **kwargs)

    def __str__(self):
        return self.file_reference

    def get_absolute_url(self):
        return reverse('batch_documents', kwargs={'file_reference': self.pk})

    @property
    def validated(self):

        return self.stage == 'Complete'

    def is_file_closed(self):
        return self.state == STATES[1]

    @staticmethod
    def who_currently_assigned(self, stage_function):

        def inner_fuction(*args, **kwargs):
            current_user = kwargs.get('user')
            if current_user and self.assigned_to == current_user:
                return stage_function(*kwargs, **kwargs)
            else:
                raise PermissionError

        return inner_fuction

    # transition methods
    @transition(field=state, source=[STATES[1]], target=STATES[0])
    def open(self, user=None):
        """"changes  file state to OPEN
       if unassigned: assigns to current user
                         """

        if self.assigned_to == user:
            pass
        elif self.assigned_to == None:
            self.assigned_to = user
            self.save()
        else:
            raise PermissionError

    @transition(field=state, source=STATES[0], target=STATES[1])
    def close(self, user):
        """"changes  file state to OPEN
               -changes assigned to null
                                 """
        if user.is_superuser:
            self.save()
        elif not self.assigned_to == user:
            raise PermissionError

        self.save()

    @transition(field=stage, source=STAGES[0], target=STAGES[1], conditions=[is_file_closed],
                permission='app.can_register_batch')
    def dispatch_reception(self, user=None):
        """"changes  file stage to RECEPTION

            -records this action in Modification Table
                                 """
        if not has_transition_perm(self.dispatch_reception, user):
            raise PermissionError
        self.modify_and_log(user=user, from_stage=STAGES[0], to_stage=STAGES[1], flagged=False)

    @transition(field=stage, source=[STAGES[1]], target=STAGES[0],
                permission=lambda instance, user: user.has_perm('app.can_receive_file'),
                conditions=[is_file_closed])
    def return_registry(self, user, rejection_comment=''):

        """"flags a  file stage to REGISTRY

                    -records this action in Modification Table
                    -notify user who created
                    -notify all admins
                               """
        if not has_transition_perm(self.return_registry, user):
            raise PermissionError
        Modification.objects.create(
            file=self,
            modified_from_stage=STAGES[1],
            modified_to_stage=STAGES[0],
            by=user
        )

        Notification.objects.create(
            file=self,
            comment=rejection_comment
        )

        self.flagged = True
        self.assigned_to = None
        self.save()

    def notify_on_return_registry(self):
        # user who created
        notification = Notification.objects.filter(file=self).last()
        try:
            NotificationSentTo.objects.create(
                notification=notification,
                user=self.file_created_by
            )

            # all admins
            for user_obj in User.objects.filter(is_superuser=True):
                if user_obj != self.file_created_by:
                    NotificationSentTo.objects.create(
                        notification=notification,
                        user=user_obj
                    )

            self.assigned_to = self.file_created_by
            self.save()
        except Exception:
            raise AttributeError

    @transition(field=stage, source=[STAGES[1]], target=STAGES[2],
                permission=lambda instance, user: instance.assigned_to == user and user.has_perm(
                    'app.can_receive_file'),
                conditions=[is_file_closed])
    def dispatch_assembly(self, user=None):

        """"changes  file stage to ASSEMBLY

                    -records this action in Modification Table
                                         """
        if not has_transition_perm(self.dispatch_assembly, user):
            raise PermissionError
        self.modify_and_log(user=user, from_stage=STAGES[1], to_stage=STAGES[2], flagged=False)

    @transition(field=stage, source=[STAGES[2]], target=STAGES[1],
                permission=lambda instance, user: instance.assigned_to == user and user.has_perm(
                    'app.can_disassemble_file'),
                conditions=[is_file_closed])
    def return_reception(self, user=None, rejection_comment=None):

        """"flags a  file stage to RECEPTION

                            -records this action in Modification Table
                            -notify user who edited at reception
                            -notify all admins
                                                 """
        if not has_transition_perm(self.return_reception, user):
            raise PermissionError
        self.modify_and_log(user=user, from_stage=STAGES[2], to_stage=STAGES[1], flagged=True,
                            kwargs={'comment': rejection_comment})

    @transition(field=stage, source=[STAGES[2]], target=STAGES[3],
                permission=lambda instance, user: instance.assigned_to == user and user.has_perm(
                    'app.can_disassemble_file'),
                conditions=[is_file_closed])
    def dispatch_scanner(self, user):
        """"changes  file stage to SCANNER

                    -records this action in Modification Table
                                         """
        if not has_transition_perm(self.dispatch_scanner, user):
            raise PermissionError
        self.modify_and_log(user=user, from_stage=STAGES[2], to_stage=STAGES[3], flagged=False)



    @transition(field=stage, source=[STAGES[3]], target=STAGES[4],
                permission=lambda instance, user: instance.assigned_to == user and user.has_perm(
                    'app.can_scan_file'),
                conditions=[is_file_closed]
                )
    def dispatch_transcriber(self, user=None):
        if not has_transition_perm(self.dispatch_transcriber, user):
            raise PermissionError
        app.signals.file_uploaded.send(sender=self.__class__,file_pk=self.pk)
        self.modify_and_log(user=user, from_stage=STAGES[3], to_stage=STAGES[4], flagged=False)

    @transition(field=stage, source=[STAGES[3]], target=STAGES[2],
                permission=lambda instance, user: instance.assigned_to == user and user.has_perm(
                    'app.can_scan_file'),
                conditions=[is_file_closed]
                )

    def return_assembly(self, user=None, rejection_comment=None):
        if not has_transition_perm(self.dispatch_transcriber, user):
            raise PermissionError
        self.modify_and_log(user=user, from_stage=STAGES[3], to_stage=STAGES[2], flagged=True,
                            kwargs={'comment': rejection_comment})

    @transition(field=stage, source=[STAGES[4]], target=STAGES[3],
                permission=lambda instance, user: instance.assigned_to == user and user.has_perm(
                    'app.can_transcribe_file'),
                conditions=[is_file_closed])
    def return_scanner(self, user=None, rejection_comment=None):

        """"flags a  file stage to SCANNER

                            -records this action in Modification Table
                            -notify user who edited at reception
                            -notify all admins
                                                 """
        if not has_transition_perm(self.return_scanner, user):
            raise PermissionError
        self.modify_and_log(user=user, from_stage=STAGES[4], to_stage=STAGES[3], flagged=True,
                            kwargs={'comment': rejection_comment})

    @transition(field=stage, source=[STAGES[4]], target=STAGES[5],
                permission=lambda instance, user: instance.assigned_to == user and user.has_perm(
                    'app.can_transcribe_file'),
                conditions=[is_file_closed])
    def dispatch_qa(self, user=None):

        """"changes  file stage to QA

                    -records this action in Modification Table
                                         """
        if not has_transition_perm(self.dispatch_qa, user):
            raise PermissionError
        self.modify_and_log(user=user, from_stage=STAGES[4], to_stage=STAGES[5], flagged=False)

    @transition(field=stage, source=[STAGES[5]], target=STAGES[4],
                permission=lambda instance, user: user.has_perm('app.can_qa_file'),
                conditions=[is_file_closed])
    def return_transcriber(self, user=None, rejection_comment=None):

        """"flags a  file stage to TRANSCRIBER

                            -records this action in Modification Table
                            -notify user who edited at reception
                            -notify all admins
                                                 """
        if not has_transition_perm(self.return_transcriber, user):
            raise PermissionError
        self.modify_and_log(user=user, from_stage=STAGES[5], to_stage=STAGES[4], flagged=True,
                            kwargs={'comment': rejection_comment})

    @transition(field=stage, source=[STAGES[5]], target=STAGES[6],
                permission=lambda instance, user: user.has_perm('app.can_qa_file'),
                conditions=[is_file_closed])
    def dispatch_validator(self, user=None):
        """"changes  file stage to VALIDATOR

                    -records this action in Modification Table
                                         """
        if not has_transition_perm(self.dispatch_validator, user):
            raise PermissionError
        self.modify_and_log(user=user, from_stage=STAGES[5], to_stage=STAGES[6], flagged=False)

    @transition(field=stage, source=[STAGES[6]], target=STAGES[5],
                permission=lambda instance, user: user.has_perm('app.can_validate_file'),
                conditions=[is_file_closed])
    def return_qa(self, user=None, rejection_comment=None):

        """"flags a  file stage to QA

                            -records this action in Modification Table
                            -notify user who edited at reception
                            -notify all admins
                                                 """
        if not has_transition_perm(self.return_qa, user):
            raise PermissionError
        self.modify_and_log(user=user, from_stage=STAGES[6], to_stage=STAGES[5], flagged=True,
                            kwargs={'comment': rejection_comment})

    @transition(field=stage, source=[STAGES[6]], target=STAGES[7],
                permission=lambda instance, user: user.has_perm('app.can_validate_file'),
                conditions=[is_file_closed])
    def dispatch_complete(self, user=None):
        if not has_transition_perm(self.dispatch_complete, user):
            raise PermissionError

        self.modify_and_log(user=user, from_stage=STAGES[6], to_stage=STAGES[7], flagged=False)

    def notify(self, from_stage, comment=''):
        notification = Notification.objects.create(
            file=self,
            comment=comment
        )
        # user who did reception
        modified = Modification.objects.filter(modified_to_stage=from_stage).last()
        if modified:
            NotificationSentTo.objects.create(
                notification=notification,
                user=modified.by
            )

        # all admins
        for user_obj in User.objects.filter(is_superuser=True):
            NotificationSentTo.objects.create(
                notification=notification,
                user=user_obj
            )
        if modified:
            self.assigned_to = modified.by
        else:
            self.assigned_to = None
        self.flagged = True
        self.save()

    def modify_and_log(self, user, from_stage, to_stage, flagged, **kwargs):
        self.flagged = flagged
        self.assigned_to = None
        self.save()
        Modification.objects.create(
            file=self,
            modified_from_stage=from_stage,
            modified_to_stage=to_stage,
            by=user)
        if flagged:
            self.notify(from_stage=from_stage, comment=kwargs['kwargs'])


class DocumentFileDetail(models.Model):
    class Meta:
        indexes = [
            BrinIndex(name='doc_doc_barcode_idx', fields=['document_barcode'], db_tablespace='pg_default')

        ]

    file_reference = models.ForeignKey(DocumentFile, db_column="file_reference", on_delete=models.CASCADE, null=True,
                                       db_index=True, db_tablespace='pg_default')
    document_barcode = models.CharField(unique=True, max_length=255, validators=[start_with_C_validator])

    document_name = models.CharField(max_length=255, blank=True)
    document_type = models.ForeignKey(DocumentType, on_delete=models.CASCADE, null=True)

    document_content = JSONField(null=True)
    document_file_path = models.CharField(null=True, max_length=100)
    doc_created_by = models.ForeignKey(User, null=True, blank=True,
                                       on_delete=models.DO_NOTHING,
                                       related_name='doc_created_by')
    created_on = models.DateTimeField(auto_now_add=timezone.now)
    flagged = models.BooleanField(default=False)
    assigned_to = models.ForeignKey(User, null=True, blank=True,
                                    on_delete=models.DO_NOTHING,
                                    related_name='doc_assigned_to')
    state = FSMField(default=STATES[0], protected=True)
    passed_qa = models.BooleanField(default=False)
    passed_validated = models.BooleanField(default=False)
    miscellaneous = JSONField(null=True)
    # transition methods
    @transition(field=state, source=[STATES[1]], target=STATES[0])
    def open(self):
        """"changes  document state to Open


                                                 """

    @transition(field=state, source=STATES[0], target=STATES[1])
    def close(self):
        """"changes  document state to Close


                                                         """
        pass

    def __str__(self):
        return self.document_barcode


class Profile(models.Model):
    class Meta:
        permissions = [('is_elderly', 'Is Elderly '),('is_guardian','Is Guardian'),('is_dead','Is ')]
    user = models.OneToOneField(User, on_delete=models.CASCADE,
                                db_index=True, db_tablespace='pg_default')
    id_no = models.CharField(null=True, max_length=25)
    phone = models.CharField(null=True, max_length=25)
    full_name = models.CharField(null=True, max_length=25)
    first_login = models.BooleanField(default=True)
    elderly = models.ForeignKey(User, null=True, on_delete=models.CASCADE, related_name='elderly')
    image = models.ImageField(default='default.jpg', upload_to='profile_pics', null=True)

    def __str__(self):
        return f'{self.user.username} Profile'

    def save(self, *args, **kwargs):
        super().save()

        img = Image.open(self.image.path)

        if img.height > 300 or img.width > 300:
            output_size = (300, 300)
            img.thumbnail(output_size)
            img.save(self.image.path)


def document_directory_path(instance, filename):
    basename = os.path.basename(filename)
    name, ext = os.path.splitext(basename)
    folder = instance.file_reference.file_reference
    batch = instance.file_reference.batch

    path = 'media/{}/{}/{}{}'.format(batch, folder, name, ext)
    return datetime.now().strftime(path)


class Filer(models.Model):
    """
    create an initial folder
    create a new folder if it has more than 2000 files inside it
    """
    filepond = models.FileField(upload_to=document_directory_path)
    file_reference = models.ForeignKey(DocumentFile, related_name='documents', on_delete=models.CASCADE)
    document_reference = models.CharField(null=True, max_length=40)

    def filename(self):
        return os.path.basename(self.filepond.name)


class Modification(models.Model):
    """ This tables all the modifications of either batch,file or document-will be used to track the action workflow"""

    class Meta:
        indexes = [
            BrinIndex(name='modification_from_to__indx', fields=['modified_from_stage', 'modified_to_stage'],
                      db_tablespace='pg_default')
        ]

    file = models.ForeignKey(DocumentFile, on_delete=models.CASCADE,
                             db_index=True, db_tablespace='pg_default')
    modified_from_stage = FSMField(null=False, protected=True)
    modified_to_stage = FSMField(null=True, protected=True)
    by = models.ForeignKey(User, on_delete=models.CASCADE,
                           db_index=True, db_tablespace='pg_default')
    created_at = models.DateTimeField(auto_now_add=timezone.now)


class Notification(models.Model):
    """all notifications"""

    file = models.ForeignKey(DocumentFile, on_delete=models.CASCADE, null=True, blank=True,
                             db_index=True, db_tablespace='pg_default')
    comment = models.TextField(null=True)
    created_at = models.DateTimeField(auto_now_add=timezone.now, null=True, blank=True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True,
                                   db_index=True, db_tablespace='pg_default')
    resolved = models.BooleanField(default=False)
    resolved_by = models.ForeignKey(User, on_delete=models.CASCADE, related_name='resolver', blank=True, null=True)


class NotificationSentTo(models.Model):
    notification = models.ForeignKey(Notification, on_delete=models.CASCADE, null=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=False)
    read_at = models.DateTimeField(null=True)


class Stock(models.Model):
    class Meta:
        permissions = [('can_manage_stock', 'Can Manage File Stock Inventory')
                       ]
        indexes = [
            BrinIndex(name='stock_file_no_indx', fields=['file_number'], db_tablespace='pg_default'),
            BrinIndex(name='stock_name_indx', fields=['name'], db_tablespace='pg_default'),
            BrinIndex(name='stock_location_indx', fields=['location_of_file'], db_tablespace='pg_default')
        ]

    FILE_LOCATIONS = [
        ('REGISTRY', 'Registry'),
        ('CITIZENSHIP', 'Citizenship'),
        ('INVESTIGATION', 'Investigation'),
        ('VISA', 'Visa'),
        ('PERMITS', 'Permits'),
        ('PERMANENT_RESIDENCE', 'Permanent Residence'),
        ('INTERVIEW', 'Interview'),
    ]
    FILE_CATEGORY = [
        ('TEMPORARY', 'Temporary'),
        ('PERMANENT', 'Permanent'),
        ('PARALLEL', 'Parallel')
    ]

    file_number = models.CharField(null=True, blank=True, max_length=50)
    comment = models.TextField(null=True, blank=True)
    name = models.CharField(max_length=80)
    nationality = models.CharField(max_length=50)
    cross_reference = models.CharField(max_length=60)
    file_category = models.CharField(max_length=60, choices=FILE_CATEGORY)
    date_last_correspondence = models.DateTimeField(null=True)
    date_first_correspondence = models.DateTimeField(null=True)
    location_of_file = models.CharField(max_length=40, choices=FILE_LOCATIONS)
    # extra additions
    created_at = models.DateTimeField(auto_now_add=timezone.now, null=True)
    updated_at = models.DateTimeField(auto_now=timezone.now, null=True)
    created_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, null=True, blank=True)

class Money_disburdment(models.Model):
    user=models.ForeignKey(User,on_delete=models.DO_NOTHING,null=False)
    created_at = models.DateTimeField(auto_now_add=timezone.now, null=True)
    sent_at = models.DateTimeField( null=True)
