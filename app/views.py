import os
import json
from pathlib import Path
from django.contrib import messages
from django.utils import timezone
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.views import PasswordChangeForm
from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.mixins import LoginRequiredMixin
import urllib.parse
from django.contrib.auth.views import LoginView
from django.views.decorators.csrf import csrf_exempt

from .models import Modification
from django.contrib.auth.models import Group, User, Permission
from django.contrib.messages.views import SuccessMessageMixin
from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import render, redirect, get_object_or_404
from django_filters.views import FilterView
from django_tables2 import RequestConfig
from django_tables2.views import SingleTableMixin

from .mixin import LoggedInRedirectMixin

from django.urls import reverse_lazy, reverse
from django.views.generic import ListView, DetailView
from django.views.generic.base import View
from django.views.generic.edit import DeleteView, CreateView, UpdateView
from django_jsonforms.forms import JSONSchemaForm
import urllib
from json2html import *

from .decorators import unauthenticated_user
from .forms import LoginForm, UserRegistrationForm, \
    PasswordResetForm, GroupCreationForm
from .models import DocumentFile, DocumentFileType, DocumentType, DocumentFileDetail, Batch, STATES, STAGES
from .tables import DocumentTable, UserTable, GroupTable, CleanUpTable
from .filters import DocumentFilter, UserFilter, GroupFilter, DocumentFileFilter
from app.view.logged_in_users import get_current_users
from .tasks import clean_up_split_files


@login_required
@permission_required('app.can_register_batch', raise_exception=True)
def edit_file(request, file_type):
    if file_type:
        file = get_object_or_404(DocumentFileType, pk=file_type)

        return render(request, 'view_document_files.html', {'file': file})


@login_required
def manage_documents(request, file_type):
    if file_type:
        file = get_object_or_404(DocumentFileType, pk=file_type)
        documents = DocumentFile.objects.filter(file_type=file_type)
        form = None
        # form = DOcumentForm()
        context = {'file': file, 'documents': documents, 'form': form}
        return render(request, 'upload_document.html', context)


class AdminView(LoginRequiredMixin, View):
    template_name = 'home.html'

    permission_required = ''

    def get(self, request):
        return render(request, self.template_name)


class FileTypeDelete(LoginRequiredMixin, SuccessMessageMixin, DeleteView):
    permission_required = 'app.delete_documentfiletype'
    model = DocumentFileType
    success_message = 'Deleted created successfully'
    success_url = reverse_lazy('list_file_types')

    def test_func(self):
        if self.request.user.profile.first_login:
            return False
        else:
            return True


class FilesView(LoginRequiredMixin, ListView):
    permission_required = 'app.add_documentfile'
    template_name = 'file/index.html'

    def get_queryset(self):
        return DocumentFile.objects.filter(batch=Batch.objects.get(pk=self.kwargs['batch_id']))


class DocumentTranscribe(LoginRequiredMixin, SingleTableMixin, FilterView):
    table_class = DocumentTable
    template_name = 'file_documents_list.html'
    filterset_class = DocumentFilter

    def get_queryset(self):
        queryset = DocumentFileDetail.objects.filter(file_reference_id=self.kwargs['file_reference']).order_by(
            'created_on')
        self.table = DocumentTable(queryset)
        self.filter = DocumentFilter(self.request.GET,
                                     DocumentFileDetail.objects.filter(
                                         file_reference_id=self.kwargs['file_reference']).order_by('created_on'))
        self.table = DocumentTable(self.filter.qs)
        RequestConfig(self.request, paginate={'per_page': 10}).configure(self.table)

    def get_context_data(self, **kwargs):
        doc_file = DocumentFile.objects.get(pk=self.kwargs['file_reference'])
        context = super().get_context_data()
        context['table'] = self.table
        context['filter'] = self.filter
        context['batch_id'] = doc_file.batch_id
        context['file_ref_no'] = self.kwargs['file_reference']

        context['file_stage'] = doc_file.stage

        return context


@login_required
def get_document_and_document_type(request, doc_id, file_type):
    document = get_object_or_404(DocumentFileDetail, pk=doc_id)
    document_path = document.document_file_path
    document_type = DocumentType.objects.get(pk=file_type)
    document_json = document_type.document_field_specs
    document_details = document.document_content

    directory = os.path.dirname(document_path)
    pages = []
    try:
        search_dir = Path("media/" + directory + "/")
        for file in os.listdir(search_dir):
            file_url = directory + "/" + file
            pages.append(file_url)

    except Exception as exc:
        pass

    # files_list = []
    store_directory = Path(document_path).parent
    storage = "media/" + str(store_directory)
    # for files in os.listdir(storage):
    #     files_list.append(files)

    if document_details is None:
        document_details = 1
    return render(request, 'transcription_lab.html',
                  {'document_json': document_json, 'document_id': document.pk, 'path': document_path,
                   'document_details': document_details, 'pages': pages})


def get_document_list(request, file_reference):
    documents = DocumentFileDetail.objects.filter(file_reference=file_reference);

    return render(request, 'files_list.html', {'documents': documents})


@login_required
@csrf_exempt
def update_document_content(request, doc_id):
    document = DocumentFileDetail.objects.get(pk=doc_id)
    try:

        json_data = json.loads(request.body)

        document.document_content = json_data

        document.save()

        return JsonResponse(status=201, data={"status": "201"})
    except Exception as exc:

        return JsonResponse(status=400, data={"status": "400"})


@login_required
def validate_document_content(request, doc_id):
    document = DocumentFileDetail.objects.get(id=doc_id)
    content = document.document_content
    document_type = document.document_type.document_name
    table_data = json2html.convert(json=content, table_attributes="id=\"info-table\" class=\"table table-bordered "
                                                                  "table-hover\"")
    return render(request, 'validate.html', {'table_data': table_data, 'document': document, 'type': document_type})


class Login(LoggedInRedirectMixin, LoginView):
    template_name = 'login.html'

    def form_valid(self, form):
        # form is valid (= correct password), now check if user requires to set own password
        if form.get_user().profile.first_login:
            return redirect(reverse_lazy('user.changepass', kwargs={'username': form.get_user().username}))

        return super().form_valid(form)


def change_password(request, username):
    user = User.objects.get(username=username)

    if request.method == 'POST':
        # user=authenticate(username=user.username, password=request.POST.get('old_password'))

        form = PasswordChangeForm(data=request.POST, user=user)

        if form.is_valid():

            form.save()
            user.refresh_from_db()
            user.profile.first_login = False
            user.save()
            # update_session_auth_hash(request, form.user)

            messages.success(request, 'Password Changed Successfully, you can now login')
            return redirect(reverse('login'))
        else:
            messages.error(request, form.error_messages)
            return redirect(reverse('user.changepass', kwargs={'username': user.username}))
    else:
        form = PasswordChangeForm(user=user)

        args = {'form': form}
        return render(request, 'reset_password.html', args)


@login_required
def password_reset(request):
    if request.method == 'POST':
        form = PasswordResetForm(user=request.user, data=request.POST)

        if form.is_valid():

            form.save()
            u = user

            u.refresh_from_db()
            u.profile.first_Login = False
            u.save()
            logout(request)
            return redirect('login')
        else:
            pass
    else:
        form = PasswordResetForm(user=request.user)
    return render(request, 'reset_password.html', {'form': form, })


@login_required
def add_group(request):
    form = GroupCreationForm(request.POST)
    if form.is_valid():
        form.save()

        messages.success(request, f"Group Created successfully")
        return redirect('groups.index')

    else:
        form = GroupCreationForm()
    return render(request, 'create_group.html', {'form': form})


class UserListView(LoginRequiredMixin, SingleTableMixin, FilterView):
    table_class = UserTable
    template_name = 'users.html'
    filterset_class = UserFilter

    def get_queryset(self):
        queryset = User.objects.all()

        user_type=self.kwargs['user_type']
        if user_type:
            int(user_type)
            if user_type == 1:
                queryset=User.objects.filter(is_superuser=True)
            if user_type ==2:
                queryset=User.objects.filter(groups__name__in=['Elderly'],is_superuser=False)
            if user_type ==3:
                queryset=User.objects.filter(groups__name__in=['Guardian'],is_superuser=False)
            if user_type ==4:
                queryset=User.objects.filter(groups__name__in=['Dead'],is_superuser=False)
        self.table = UserTable(queryset)
        self.filter = UserFilter(self.request.GET,
                                 queryset)
        self.table = UserTable(self.filter.qs)

        RequestConfig(self.request, paginate={'per_page': 10}).configure(self.table)

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context['table'] = self.table
        context['filter'] = self.filter
        context['user_type']=self.kwargs['user_type']
        return context


class LoggedInUserListView(LoginRequiredMixin, SingleTableMixin, FilterView):
    table_class = UserTable
    template_name = 'logged_in_users.html'
    filterset_class = UserFilter

    def get_queryset(self):
        queryset = get_current_users()
        self.table = UserTable(queryset)
        self.filter = UserFilter(self.request.GET,
                                 queryset)
        self.table = UserTable(self.filter.qs)

        RequestConfig(self.request, paginate={'per_page': 10}).configure(self.table)

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context['table'] = self.table
        context['filter'] = self.filter
        return context


class UserDetailView(LoginRequiredMixin, DetailView):
    permission_required = 'auth.view_user'

    model = User
    template_name = 'user_details.html'


class GroupListView(LoginRequiredMixin, SingleTableMixin, FilterView):
    table_class = GroupTable
    template_name = 'groups.html'
    filterset_class = GroupFilter

    def get_queryset(self):
        queryset = Group.objects.all()
        self.table = GroupTable(queryset)
        self.filter = GroupFilter(self.request.GET,
                                  Group.objects.all())
        self.table = GroupTable(self.filter.qs)

        RequestConfig(self.request, paginate={'per_page': 10}).configure(self.table)

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context['table'] = self.table
        context['filter'] = self.filter
        return context


class UserUpdateView(LoginRequiredMixin, UpdateView):
    permission_required = 'auth.change_user'

    model = User
    success_url = '/profile/'

    fields = ['username', 'email', 'groups', 'is_superuser']
    template_name = 'edit_user.html'


class GroupUpdateView(LoginRequiredMixin, UpdateView):
    permission_required = 'auth.change_group'

    model = Group
    success_url = '/roles/'

    fields = ['name', 'permissions']
    template_name = 'create_group.html'


class UserDeleteView(LoginRequiredMixin, DeleteView):
    permission_required = 'auth.delete_user'

    model = User
    success_url = '/profile/'
    template_name = 'user/delete_confirm.html'


class GroupDeleteView(LoginRequiredMixin, DeleteView):
    permission_required = 'auth.delete_user'

    model = Group
    success_url = '/roles/'
    template_name = 'group_delete_confirm.html'


@login_required
def user_create(request):
    if request.method == 'POST':
        form = UserRegistrationForm(request.POST)
        if form.is_valid():
            id_no = form.cleaned_data.get('id_no')
            email = form.cleaned_data.get('email')
            username = id_no
            password = id_no
            groups = form.cleaned_data.get('groups')
            is_superuser = request.POST.get('is_superuser')
            user = None
            if is_superuser:

                user = User.objects.create_superuser(username, email, password)
            else:
                user = User.objects.create_user(username, email, password)
                user.groups.set(groups)
            user.refresh_from_db()
            user.profile.id_no = form.cleaned_data.get('id_no')
            user.profile.phone = form.cleaned_data.get('phone')
            user.profile.full_name = form.cleaned_data.get('full_name')
            user.save()
            messages.success(request, f'User created successfully!')
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

    else:
        form = UserRegistrationForm()
    return render(request, 'create_user.html', {
        'groups': Group.objects.all(),
        'form': form,
    })


@login_required
def group_create(request):
    return render(request, 'create_group.html')


class GroupCreateView(LoginRequiredMixin, CreateView):
    model = Group

    permission_required = 'auth.view_group'
    permissions = Permission.objects.all()
    extra_context = permissions
    template_name = 'create_group.html'
    fields = ['name', 'permission']
    success_url = 'home'


@login_required
def registry_batch_submit(request, batch_id):
    batch = Batch.objects.get(pk=batch_id)
    if batch and int(batch.state.state_code) <= 300:
        files = DocumentFile.objects.filter(batch=batch)
        docs = DocumentFileDetail.objects.filter(file_reference__in=files)

        new_state = 301
        if files and docs:
            try:
                docs.update(state_id=new_state, )
                files.update(state_id=new_state)
                batch.state_id = new_state
                batch.save()
                messages.success(request, 'Submitted successfully')
            except AttributeError as e:
                messages.error(request, ' something wrong happened')

        else:
            messages.warning(request, 'Empty batch or files ')
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


@login_required
def receiver_batch_submit(request, batch_id):
    batch = Batch.objects.get(pk=batch_id)
    if batch and int(batch.state.state_code) <= 301:
        files = DocumentFile.objects.filter(batch=batch)
        docs = DocumentFileDetail.objects.filter(file_reference__in=files)

        new_state = 302
        desc = None
        if request.POST.get('desc') != '':
            new_state = 401
            desc = request.POST.get('desc')

        try:
            docs.update(state_id=new_state, )
            files.update(state_id=new_state)
            batch.state_id = new_state
            batch.rejection_by_receiver_dec = desc
            batch.received_on = timezone.now()
            batch.save()
            messages.success(request, 'Submitted successfully')
        except AttributeError as e:
            messages.error(request, ' something wrong happened')

    return redirect('batch_index')


def get_file(request, file_ref=None):
    if not file_ref == None:
        file = DocumentFile.objects.get(pk=file_ref)

        if file:
            return file

    return None


def get_docs_from_file(request, file):
    docs = DocumentFileDetail.objects.filter(file_reference=file)
    if docs:
        return docs
    return None


@login_required
def request_file(request):
    if request.user.has_perm('app.can_transcribe_file'):

        file = DocumentFile.objects.filter(state_id=305, assigned_to=None).first()
        if file:
            try:
                returned_object_type = ContentType.objects.get(app_label='app', model='documentfile')
                file.assigned_to = request.user
                file.save()
                Modification.objects.create(object_type=returned_object_type, object_pk=file.pk,
                                            modified_from_state=file.state, by=request.user)
                messages.success(request, 'New file given')

            except AttributeError as e:
                messages.error(request, ' something wrong happened')
        else:
            messages.warning(request, 'No files Available')
    else:
        messages.error(request, "Don't have this permission")
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


def abort(request):
    return render(request, 'app/others/lock_screen.html')


def file_submit(request, file_ref):
    file = get_file(request, file_ref)

    docs = get_docs_from_file(request, file)

    desc = None
    if request.POST.get('desc') != None:
        desc = request.POST.get('desc')
        change_state(request, file, docs, True, desc)
    else:
        change_state(request, file, docs, False, desc)
    messages.success(request, 'Updated Successfull')
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


def change_state(request, file=None, docs=None, is_reject=None, desc=None):
    file = file
    docs = docs

    if file:
        # file and docs
        current_state_code = int(file.state.state_code)
        desc = desc
        new_state = None

        if current_state_code == 302 and file.file_scanned_by == request.user:

            docs.update(state_id=303,
                        scanned_on=timezone.now()
                        )

            file.state_id = 303
            file.scanned_on = timezone.now()
            file.save()
            messages.success(request, 'File Updated successfully')
            return redirect(request, 'list_document_files')
        elif current_state_code == 303 and file.file_transcribed_by == request.user:
            messages.success(request, 'File Updated successfully')

        elif current_state_code == 303 and file.file_transcribed_by == request.user:
            docs.update(state=new_state,
                        transcribed_on=timezone.now(),
                        rejection_by_transcriber_dec=desc
                        )
            file.state = new_state
            file.transcribed_on = timezone.now()
            file.rejection_by_transcriber_dec = desc
            file.save()

        elif current_state_code == 304 and not file.file_qa_by:
            docs.update(state=new_state,
                        qa_on=timezone.now(),
                        rejection_by_qa_dec=desc
                        )
            file.state = new_state
            file.file_qa_by = request.user
            file.qa_on = timezone.now()
            file.rejection_by_qa_dec = desc
            file.save()
        elif current_state_code == 305 and not file.file_validated_by:
            docs.update(state=new_state,
                        validated_on=timezone.now(),
                        rejection_by_validation_dec=desc
                        )
            file.state = new_state
            file.file_validated_by = request.user
            file.validated_on = timezone.now()

            file.rejection_by_validation_dec = desc
            file.save()

    else:
        messages.warning(request, 'Empty file not allowed')
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


def start_receive(request, batch_id):
    batch = Batch.objects.get(pk=batch_id)

    if batch and batch.state.state_code == '301' and batch.received_by == None:
        try:
            batch.received_by = request.user
            batch.save()
            return redirect(reverse_lazy('files.view', kwargrs={'batch_id': batch_id}))
        except AttributeError as e:
            messages.error(request, ' something wrong happened')
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


def start_scanning(request, file_ref):
    file = get_file(request, urllib.parse.unquote(file_ref))

    if file and file.state.state_code == '302' and file.file_scanned_by == None:

        docs = get_docs_from_file(request, file)

        try:
            docs.update(
                doc_scanned_by=request.user
            )

            file.file_scanned_by = request.user
            file.save()
            return render(request, 'upload_document.html', {'file': file})
        except AttributeError as e:
            messages.error(request, ' something wrong happened')
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


def start_qa(request, file_ref):
    file = get_file(request, urllib.parse.unquote(file_ref))
    if file:

        docs = get_docs_from_file(request, file)

        try:
            docs.update(
                doc_qa_by=request.user
            )
            file.file_qa_by = request.user
            file.save()
            return render(request, 'upload_document.html', {'file': file})
        except AttributeError as e:
            messages.error(request, ' something wrong happened')
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


def start_validate(request, file_ref):
    file = get_file(request, urllib.parse.unquote(file_ref))

    if file and file.state.state_code == '305' and file.file_validated_by == None:

        docs = get_docs_from_file(request, file)

        try:
            docs.update(
                doc_validated_by=request.user
            )

            file.file_validated_by = request.user
            file.save()
            return render(request, 'upload_document.html', {'file': file})
        except AttributeError as e:
            messages.error(request, ' something wrong happened')
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


def registry_submit(request, batch_id):
    batch = Batch.objects.get(pk=batch_id)
    print(batch)
    batch.state_id = 301
    batch.save()
    messages.success(request, 'Submitted successfully')
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


def handler404(request, exception):
    return render(request, '404.html', status=404)


def handler500(request):
    return render(request, '500.html', status=500)


def close_state_of_files(request):
    user = request.user
    check = request.POST.getlist('check')

    for id in check:
        file = get_object_or_404(DocumentFile, pk=id)
        if file.state == STATES[1]:
            pass
        else:
            file.close(user=user)
            file.save()
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


def open_state_of_files(request):
    user = request.user
    check = request.POST.getlist('check')

    for id in check:
        file = get_object_or_404(DocumentFile, pk=id)
        if file.state == STATES[0]:
            pass
        else:
            file.open(user=user)
            file.save()
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


def unassign_files_from_users(request):
    check = request.POST.getlist('check')

    for id in check:
        file = get_object_or_404(DocumentFile, pk=id)
        file.assigned_to = None
        file.save()
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


def null_page(request):
    return render(request, 'blank.html', )


class CleanSplit(LoginRequiredMixin, SingleTableMixin, FilterView):
    permission_required = 'app.view_documentfile'
    filterset_class = DocumentFileFilter
    table_class = CleanUpTable
    template_name = 'split/index.html'

    def get_queryset(self):
        queryset = DocumentFile.objects.all().order_by('created_on')
        self.table = CleanUpTable(queryset)
        self.filter = DocumentFileFilter(self.request.GET,
                                         queryset)
        self.table = CleanUpTable(self.filter.qs)
        RequestConfig(self.request, paginate={'per_page': 100}).configure(self.table)
        # return Batch.objects.filter(is_return_batch=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context['table'] = self.table
        context['filter'] = self.filter
        return context


def start_process(request, file_reference):
    file = DocumentFile.objects.filter(stage=STAGES[4]).get()
    if file:
        clean_up_split_files(file_reference)
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
