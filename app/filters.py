import django_filters
from django.contrib.auth.models import User, Group
from .models import DocumentFile, DocumentFileDetail, Batch, Stock,Money_disburdment


class DocumentFileFilter(django_filters.FilterSet):
    class Meta:
        model = DocumentFile
        fields = {
            'file_reference': ['icontains'],
            'file_barcode': ['icontains'],
            'stage':['exact']
        }


class DocumentFilter(django_filters.FilterSet):
    class Meta:
        model = DocumentFileDetail
        fields = {
            'document_barcode': ['icontains'],
            'document_type': ['exact'],
        }


class BatchFilter(django_filters.FilterSet):
    class Meta:
        model = Batch
        fields = {
            'batch_no': ['icontains'],
            'description': ['icontains']
        }

class StockFilter(django_filters.FilterSet):
    class Meta:
        model = Stock
        fields = {'name': ['icontains'],
                  'file_number': ['icontains'],
                  'location_of_file': ['icontains']
                  }


class UserFilter(django_filters.FilterSet):
    class Meta:
        model = User
        fields = {
            'username': ['icontains'],
            'email': ['icontains']
        }

class DispatchFilter(django_filters.FilterSet):
    class Meta:
        model = Money_disburdment
        fields = {
            'created_at': ['icontains'],

        }
class GroupFilter(django_filters.FilterSet):
    class Meta:
        model = Group
        fields = {'name':['icontains']}
