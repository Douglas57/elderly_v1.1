from django.db import connection


def sql_logger(actions):
    if actions is not None:
        f = open('sql.txt', 'a+')
        f.write("%-40s %6s\n" % (actions, connection.queries))
        f.close()
    print(connection.queries)
