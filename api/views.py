from rest_framework.permissions import IsAuthenticated
from rest_framework import views
from rest_framework.parsers import MultiPartParser, FormParser, FileUploadParser
from rest_framework.response import Response
from rest_framework import status
import shutil
from rest_framework.decorators import api_view, renderer_classes
from rest_framework.renderers import JSONRenderer, TemplateHTMLRenderer
from rest_framework.views import APIView


from django.core.files.storage import default_storage
from django.core.files.base import ContentFile
from rest_framework.generics import (
    ListAPIView,
    RetrieveAPIView,
    CreateAPIView,
    UpdateAPIView,
    DestroyAPIView)
from django.conf import settings

from app.forms import StorageForm
from app.models import DocumentFile, STAGES, Filer
from .serializer import DocumentFileSerializer, PictureSerializer


# Create your views here.
@api_view(('GET',))
def search_barcode(request, barcode):
    file = DocumentFile.objects.get(file_barcode=barcode)
    serializer = DocumentFileSerializer(file)
    if file:
        return Response(serializer.data, status=200)
    else:
        return Response(status=404, )


class DocumentFileDetailView(RetrieveAPIView):
    lookup_field = "file_barcode"
    lookup_url_kwarg = "file_barcode"
    permission_classes = (IsAuthenticated,)
    queryset = DocumentFile.objects.filter(stage=STAGES[4])
    serializer_class = DocumentFileSerializer


class FileUploadView(APIView):
    parser_class = (FileUploadParser,)

    def post(self, request, *args, **kwargs):
        file_barcode = request.data['file_barcode']
        image = request.FILES['filepond']
        if file_barcode is not None:
            file = DocumentFile.objects.filter(file_barcode=file_barcode).first()
            try:
                data = {
                    'file_reference': file.pk,
                    'filepond': image
                }
                file_serializer = PictureSerializer(data=data)
                if file_serializer.is_valid():
                    file_serializer.save()
                    return Response(file_serializer.data, status=status.HTTP_201_CREATED)
                else:
                    print(file_serializer.errors)
                    return Response(file_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
            except Exception as exc:
                return Response(file_serializer.errors, status=status.HTTP_400_BAD_REQUEST)


def file_store_conversion():
    pass