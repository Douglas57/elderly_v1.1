from rest_framework import serializers
from app.models import DocumentFile, Filer


class DocumentFileSerializer(serializers.ModelSerializer):
    class Meta:
        model = DocumentFile
        fields = '__all__'


class PictureSerializer(serializers.ModelSerializer):
    class Meta:
        model = Filer
        fields = ('filepond','file_reference',)
